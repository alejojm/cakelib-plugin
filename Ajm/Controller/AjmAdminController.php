<?php
/**
 * Application Lib Controller
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 * @author Alejo JM <alejo.jm@gmail.com> April 15, 2015
 *
 */
class AjmAdminController extends Controller {
	/**
	 * setup the Auth comp
	 * @var    array
	 */
	public $components = array(
		'Acl','Session',
		'Auth' => array(
			'authorize' => array(
				'Actions' => array(
					'actionPath' => 'controllers',
					'userModel'  => 'Login',
				),
			),
			'authenticate' => array(
				'Ajm.Adminform' => array(
					'userModel' => 'Login',
				)
			)
		),
	);

	/**
	 * setup Auth component, and our own vars
	 * @return [type] [description]
	 * @author Alejo  JM            <alejo.jm@gmail.com> August 22, 2013
	 */
	public function beforeFilter() {
		// Configure AuthComponent redirections and error message according with login type
		$this->Auth->authenticate['Ajm.Adminform']['fields']['username'] = Configure::read('Login.field.username') ? Configure::read('Login.field.username') : 'username';
		$this->Auth->authenticate['Ajm.Adminform']['fields']['password'] = Configure::read('Login.field.password') ? Configure::read('Login.field.password') : 'password';
		$this->Auth->loginAction    = array('controller' => 'logins', 'action' => 'users', 'plugin' => $this->params['plugin']);
		$this->Auth->logoutRedirect = array('controller' => 'logins', 'action' => 'users', 'plugin' => $this->params['plugin']);
		$this->Auth->authError      = __d('ajm', "You don't have permission to perform this action");

		if ($this->Auth->loggedIn()) {
			// Create menu for the logged in user
			$this->loadModel('Ajm.Menu');
		}
	}

	/**
	 * before render the logic in the controllers add the menu
	 * @return void
	 */
	public function beforeRender()
	{
		if(isset($this->Menu)){
			$this->beforeRenderMenu();
			$this->Menu->setup($this->Acl, $this->name, $this->params['plugin']);
			$itemsMenu = $this->Menu->getItemsForUser();
			$this->set('itemsMenu', $itemsMenu);
		}
	}

	/**
	 * before render the menu, you can disable items with the methods
	 * 		Menu::removeClassMethods('ControllerName', array('action'));
	 *   	Menu::removeActions(array('action'));
	 * @return void
	 */
	public function beforeRenderMenu(){
	}

}

