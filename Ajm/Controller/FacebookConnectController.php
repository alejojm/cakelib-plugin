<?php


/**
* Facebook controller
*/
class FacebookConnectController extends AppController {
	/**
	 * facebook app id
	 * @var    [type]
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	protected $appId       = null;

	/**
	 * facebook app secret
	 * @var    [type]
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	protected $secret      = null;

	/**
	 * what permissions we need
	 * https://developers.facebook.com/docs/reference/login/
	 * @var    string
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	protected $permissions = 'email';

	/**
	 * used when apache proxy is making the request
	 * @var    [type]
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	protected $basePath = '';

	/**
	 * the token name used for save the session
	 * @var    string
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	protected $tokenName = 'facebok_access_token';

	/**
	 * facebook grap url
	 * @var    string
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	private $graph = 'https://graph.facebook.com/v2.2';

	/**
	 * the url to send to facebook
	 * @var    [type]
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	private $redirect_uri;

	/**
	 * Cake socket
	 * @var    [type]
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	private $HttpSocket;

	/**
	 * what host will be used for redirect
	 * @var    string
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	private $redirect_host;

	/**
	 * the queryString name "from", used when the proccess is complete
	 * @var    string
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	private $internal_redirect = '';

	/**
	 * we need cookie component, to store thing cross application
	 * @var    array
	 * @author Alejo JM <alejo.jm@gmail.com> December 10, 2013
	 */
	public $components = array('Session','Cookie');

	/**
	 * redirect to the facebook
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> September 11, 2013
	 */
	function beforeFilter() {
		parent::beforeFilter();

		App::uses('HttpSocket', 'Network/Http');
		$this->HttpSocket   = new HttpSocket();
		$this->redirect_uri = $this->url.$this->basePath.'/facebook/user/';
		$this->redirect_host = $this->proto.$this->host;

		//setup the cookie
		$this->Cookie->name = 'FacebookConnect';

		//we have a proxy request use the proxy server
		if(!empty($this->forwarded_server)){
			$this->redirect_host = $this->proto.$this->forwarded_host;
			$this->redirect_uri = $this->proto.$this->forwarded_host.$this->webroot.$this->basePath.'/facebook/user/';
		}

		$this->internal_redirect = $this->getInternalRedirect();

		if(is_null($this->appId))
			throw new Exception($this->name . ' appId is null, copy paste from https://developers.facebook.com/apps/$appId/dashboard/');

		if(is_null($this->secret))
			throw new Exception($this->name . ' secret is null, copy paste from https://developers.facebook.com/apps/$appId/dashboard/');

	}

	/**
	 * Redirect the browser to the facebook world
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	final public function login()
	{
		$this->saveInternalRedirect();
		$this->redirect("https://www.facebook.com/dialog/oauth/?client_id={$this->appId}&redirect_uri={$this->redirect_uri}&scope={$this->permissions}");
	}

	/**
	 * Redirect the browser to the facebook world
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	final public function logout()
	{
		//we don't have token
		if(!$this->getAccessToken()){
			$redirect = $this->redirectFromError('facebook_logout_fail');
			return $this->redirect(@$this->params->query['from'].$var.'facebook_logout_fail');
		}
		$this->saveBoth('facebook_is_logout', true);
		$this->saveInternalRedirect();
		$this->redirect("https://www.facebook.com/logout.php?next={$this->redirect_uri}&access_token=".$this->getAccessToken());
	}

	/** TODO
		this only work if we save thing in the database, like
		access_token, facebook userId
	 * clear all session when the user remove the app from facebook
	 * you must add this url en facebook ui in
	 * https://developers.facebook.com/apps/{appId}/advanced
	 * http://www.example.com/facebook/logout/
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	// public function logout()
	// {
	// 	$this->log(array_merge(array('---------- logout'), $_SERVER, $this->data), 'debug');
	// 	$this->Session->delete('internal_redirect');
	// 	$this->Session->delete($this->tokenName);
	// 	$this->Session->delete('facebook_user');
	// }

	/**
	 * clear all session stuff, from inside
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	final public function clear()
	{
		$this->deleteBoth('internal_redirect');
		$this->deleteBoth($this->tokenName);
		$this->deleteBoth('facebook_user');
	}

	/**
	 * the user can accept the permisions or deny, get the user data and redirect
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	final public function user(){
		//is logout
		if($this->getFromSession('facebook_is_logout')){
			$this->deleteBoth('facebook_is_logout');
			$this->clear();
			return $this->redirectLogout();
		}

		$query = $this->params->query;
		if ((isset($query['error']) || isset($query['error_reason'])) || !isset($query['code'])) {
			$this->redirectFromError();
			exit;
		}

		$fbUrlParams = "client_id=" . $this->appId . "&client_secret=" . $this->secret;
		$fbUrlParams .= "&redirect_uri=".$this->redirect_uri;
		$fbUrlParams .= "&code=" . $query['code'];

		//always get the access token from facebook, we don't want use some old token
		$response = $this->HttpSocket->get($this->graph . "/oauth/access_token?" . $fbUrlParams);
		$get      = null;
		parse_str($response['body'], $get);
		if(!isset($get['access_token'])){
			$this->redirectFromError();
		}	else 	{
			$this->saveBoth($this->tokenName, $get['access_token']);
			$this->saveUserData();
		}
	}

	/**
	 * using the access_token from facebook we can get the info
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	final public function saveUserData()
	{
		$access_token = $this->Session->read($this->tokenName);
		$response = $this->HttpSocket->get($this->graph . "/me?access_token=" . $access_token);
		$user = json_decode($response['body'], true);

		//something is wrong, refresh the entire process
		if(isset($user['error'])){
			$this->clear();
			$this->redirectFromError();
		}

		$this->Session->write('facebook_user', $user);
		$this->redirectSuccess();
	}

	/**
	 * redirect the error, facebook fail for some reason
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	final public function redirectFromError($name=null)
	{
		$redirect = $this->getRedirectUrl(!$name ? 'facebook_connect_fail' : $name );
		$urlRedirectFail = $this->redirect_host.'/'.$redirect;

		$newRedirect = $this->beforeRedirectError($urlRedirectFail);
		if($newRedirect){
			$urlRedirectFail = $newRedirect;
		}

		$this->redirect($urlRedirectFail);
	}

	/**
	 * facebook connect was success
	 * @param  string $urlRedirectSuccess
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	final public function redirectSuccess()
	{
		$redirect = $this->getRedirectUrl('facebook_connect_success');
		$urlRedirectSuccess = $this->redirect_host.'/'.$redirect;
		$newRedirect = $this->beforeRedirectSuccess($urlRedirectSuccess);
		if($newRedirect){
			$urlRedirectSuccess = $newRedirect;
		}

		$this->redirect($urlRedirectSuccess);
	}

	/**
	 * facebook logout was success
	 * @param  string $urlRedirectSuccess
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	final public function redirectLogout()
	{
		$redirect = $this->getRedirectUrl('facebook_logout_success');
		$urlLogoutSuccess = $this->redirect_host.'/'.$redirect;
		$newRedirect = $this->beforeRedirectLogout($urlLogoutSuccess);
		if($newRedirect){
			$urlLogoutSuccess = $newRedirect;
		}

		$this->redirect($urlLogoutSuccess);
	}

	/**
	 * get user data from facebook world
	 * @return array
	 * @author Alejo JM <alejo.jm@gmail.com> December 10, 2013
	 */
	final public function getUserData()
	{
		$me = $this->getGrapApi('/me/');
		if(empty($me) || isset($me['error'])){
			$this->clear();
			return false;
		}

		return $me;
	}

	/**
	 * get the user friends with the token
	 * @return array
	 * @author Alejo JM <alejo.jm@gmail.com> October 17, 2013
	 */
	final public function getUserFriends(){
		$friends = $this->getGrapApi('/me/friends/');

		//something is wrong, refresh the entire process
		if(isset($friends['error'])){
			$this->clear();
			return false;
		}

		return $friends;
	}

	/**
	 * make a feed on facebook user friend
	 * @deprecated This method don't work,
	 * from php see: http://stackoverflow.com/questions/14056046/feed-story-publishing-to-other-users-is-disabled-for-this-application
	 * @param  string $friendId facebook friend id
	 * @param  array  $message  array('link'=>'string', name=>'string', caption=>'string')
	 * @return true/false
	 * @author Alejo JM <alejo.jm@gmail.com> October 18, 2013
	 */
	// final public function addUserFriendFeed($friendId, $message=array()){
	// 	if(!$this->facebookSdk)
	// 		throw new Exception('FacebookConnectController missing php facebook SDK, include the SDK inside app/Vendor directory');
	//
	// 	$this->facebookSdk->setAccessToken($this->Session->read($this->tokenName));
	// 	$response = $this->facebookSdk->api("/$friendId/feed", 'post', $message);
	// }
	// OR
	// final public function addUserFriendFeed($friendId, $message=array()){
	// 	$response = $this->postGrapApi("/$friendId/feed", $message);
	// }

	/**
	 * get permissions for the user
	 * @return array array with the permisions, ex:array("installed": 1, "basic_info": 1, "email": 1, "publish_actions": 1, "user_friends": 1)
	 * @author Alejo JM <alejo.jm@gmail.com> October 18, 2013
	 */
	final public function getUserPermissions(){
		$response = $this->getGrapApi('/me/permissions/');

		if(isset($response['error'])){
			$this->clear();
			return false;
		}

		return isset($response['data'][0]) ? $response['data'][0] : false;
	}

	/**
	 * remove permision from facebook for this app
	 * @return vpoid [description]
	 * @author Alejo JM <alejo.jm@gmail.com> December 16, 2014
	 */
	final public function deletePermisions(){
		$access_token = $this->getAccessToken();
		if(empty($access_token))
			return false;

		$response = $this->HttpSocket->delete($this->graph . '/me/permissions?access_token=' . $access_token);

		if(isset($response['error'])){
			$this->clear();
			return false;
		}

		return true;
	}



	/**
	 * get if this user need like some page_id
	 * @param  string $page_id
	 * @return true/false
	 * @author Alejo JM <alejo.jm@gmail.com> December 10, 2013
	 */
	final public function getUserLikePage($page_id){
		$response = $this->getGrapApi('/me/likes/'.$page_id);

		if(isset($response['error'])){
			$this->clear();
			return false;
		}

		return isset($response['data'][0]) ? $response['data'][0] : false;
	}

	/**
	 * generic GET call to the graph api
	 * @param  string $url the url for use like /me/friends/
	 * @return array
	 * @author Alejo JM <alejo.jm@gmail.com> October 18, 2013
	 */
	final public function getGrapApi($url)
	{
		$access_token = $this->getAccessToken();
		if(empty($access_token))
			return false;
		$get = $this->HttpSocket->get($this->graph . $url . "?access_token=" . $access_token);
		$response = json_decode($get['body'], true);
		return $response;
	}

	/**
	 * generic POST call to the graph api
	 * @param  string $url the url for use like /me/feed/
	 * @param  array $data array with fields for send to the api
	 * @return array
	 * @author Alejo JM <alejo.jm@gmail.com> October 18, 2013
	 */
	final public function postGrapApi($url, $data)
	{
		$access_token = $this->getAccessToken();
		if(empty($access_token))
			return false;
		$post = $this->HttpSocket->post($this->graph. $url . '?access_token='.$access_token, $data);
		$response = json_decode($post['body'], true);
		return $response;
	}


	/**
	 * load the user data stored in the session
	 * @return array
	 * @author Alejo JM <alejo.jm@gmail.com> October 03, 2013
	 */
	final public function readUserData()
	{
		return $this->Session->read('facebook_user');
	}

	/**
	 * get access token from session or cookie
	 * @return string
	 * @author Alejo JM <alejo.jm@gmail.com> October 19, 2013
	 */
	final public function getAccessToken()
	{
		$tokenSession = $this->Session->read($this->tokenName);
		if($tokenSession)
			return $tokenSession;

		$tokenCookie = $this->Cookie->read($this->tokenName);
		//is in the cookie remove from there and save in the session
		if($tokenCookie){
			$this->Session->write($this->tokenName, $tokenCookie);
			$this->Cookie->delete($this->tokenName);
			return $tokenCookie;
		}

		return false;
	}

	/**
	 * save key and value in both session and cookie
	 * @param  string $key
	 * @param  mixed $value
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> December 11, 2013
	 */
	private final function saveBoth($key, $value)
	{
		$this->Session->write($key, $value);
		$this->Cookie->write($key, $value);
	}

	/**
	 * clean key from both session and coookie
	 * @param  string $key
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> December 11, 2013
	 */
	private final function deleteBoth($key)
	{
		$this->Session->delete($key);
		$this->Cookie->delete($key);
	}

	/**
	 * clean key from both session and coookie
	 * @param  string $key
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> December 11, 2013
	 */
	private final function getFromSession($key)
	{
		if($this->Session->read($key))
			return $this->Session->read($key);
		if($this->Cookie->read($key))
			return $this->Cookie->read($key);

		return false;
	}

	/**
	 * save the internal_redirect in the cookie and session, will be available
	 * from browser or applicaction
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> December 11, 2013
	 */
	private final function saveInternalRedirect()
	{
		$this->saveBoth('internal_redirect', $this->params->query);
	}

	/**
	 * keep all others params in the url
	 * @param  string $type facebook_connect_success, facebook_connect_fail
	 * @return string
	 * @author Alejo JM <alejo.jm@gmail.com> April 30, 2014
	 */
	private final function getRedirectUrl($type)
	{
		$redirect = '';
		if(isset($this->internal_redirect['from'])){
			$redirect .= $this->internal_redirect['from'];
			unset($this->internal_redirect['from']);
		}

		$redirect .= Router::queryString($this->internal_redirect, array($type=>''));
		//remove first slash
		$redirect = substr($redirect, 0, 1) == '/' ? substr($redirect, 1, strlen($redirect)) : $redirect;
		return $redirect;
	}

	/**
	 * get internal redirect
	 * @return string
	 * @author Alejo JM <alejo.jm@gmail.com> December 11, 2013
	 */
	private final function getInternalRedirect()
	{
		$internalSession = $this->Session->read('internal_redirect');
		if($internalSession)
			return $internalSession;
		$internalCookie = $this->Cookie->read('internal_redirect');
		if($internalCookie)
			return $internalCookie;

		return '';
	}

	/**
	 * override in your class, you can change the url was render
	 * @param  $url
	 * @return false	in your class must return a url string
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	public function beforeRedirectError($url)
	{
		return false;
	}

	/**
	 * override in your class, you can change the url was render
	 * @param  $url
	 * @return false	in your class must return a url string
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	public function beforeRedirectSuccess($url)
	{
		return false;
	}

	/**
	 * override in your class, you can change the url was render
	 * @param  string $url
	 * @return false      in your class must return a url string
	 * @author Alejo JM <alejo.jm@gmail.com> October 22, 2014
	 */
	public function beforeRedirectLogout($url)
	{
		return false;
	}



}


?>