<?php
/**
* Component to upload images
*/

class ImagesComponent extends Component
{
	var $controller = true;

	function initialize(Controller $controller) {
		// saving the controller reference for later use
		$this->controller =& $controller;
	}

	function isUniqueFile($path, $fileName)
	{
		//hash the name and test is unique
		$extesion = pathinfo($fileName, PATHINFO_EXTENSION);
		$hashName = substr(md5(uniqid(rand())),0,10).time().'.'.$extesion;

		if(file_exists($path . 'size1_' .$hashName))
			return $this->isUniqueFile($path, $filename);
		else
			return $hashName;
	}

	function validFileName ($filename) {
		//get the extension off the file
		$ext    = strtolower(substr($filename,strrpos($filename,'.')+1));

		$temp = $filename;
		$temp = strtolower($temp);// Lower case
		$temp = str_replace(' ', '', $temp);//spaces the extension
		$temp = str_replace($ext, '', $temp);//remove the extension

		// Loop through string
		$result = '';
		for ($i=0; $i<strlen($temp); $i++)
		{
			if (preg_match('([0-9]|[a-z]|_)', $temp[$i]))//only alphaNumeric
				$result = $result . $temp[$i];
		}

		// Return filename
		return $result.'.'.$ext;
	}

	function delete($fileName)
	{
		$path = WWW_ROOT . Configure::read('path.uploaded.images');
		$path = str_replace('//', '/', $path);
		return unlink($path . $fileName);
	}

	/**
	 * take the bynary file encoded in base64 and generate the file
	 * @param  array $file
	 * @param  array $options
	 * @return array
	 * @author Alejo JM <alejo.jm@gmail.com> January 05, 2015
	 */
	public function uploadBinary($file, $options)
	{
		if(!isset($file['content']))
			return array('error'=>true, 'type'=>'no_file');

		$path = isset($options['path']) ? $options['path'] : Configure::read('path.uploaded.images');
		$fileName = tempnam(WWW_ROOT.$path, '');
		file_put_contents($fileName, base64_decode($file['content']));

		$file['tmp_name'] = $fileName;
		$uploaded = $this->upload($file, $options);
		@unlink($fileName);
		return $uploaded;
	}

	function upload($file, $options=array())
	{
		if(!isset($file['tmp_name']))
			return array('error'=>true, 'type'=>'no_file');
		if(!is_file($file['tmp_name']))
			return array('error'=>true, 'type'=>'no_file');

		$return    = array('error'=>false);
		$width     = explode(' ',$options['width']);//explode by spaces
		$height    = explode(' ',$options['height']);//explode by spaces
		$imageInfo = getimagesize($file['tmp_name']);
		$tempImage = '';

		$fileSize = filesize($file['tmp_name']);
		//we can proccess the image ?
		if(!$this->checkMemory($file))
			return array('error'=>true, 'type'=>'memory_limit');

		//rename file if a file with the same name already exists in the server.
		$path = isset($options['path']) ? $options['path'] : Configure::read('path.uploaded.images');

		//we send limit upload file
		if(isset($options['limit'])){
			$limit = $this->convertBytes($options['limit']);
			if($fileSize > $limit)
				return array('error'=>true, 'type'=>'upload_limit');
		}

		//php can upload the file ?
		$upload_max_filesize = $this->convertBytes(ini_get('upload_max_filesize'));
		if($fileSize > $upload_max_filesize)
			return array('error'=>true, 'type'=>'upload_limit');


		$newName = $this->isUniqueFile(WWW_ROOT . $path, $file['name']);
		//load the image from the tmp_name
		if(!$tempImage = imagecreatefromstring(file_get_contents($file['tmp_name'])))
			$return = array('error'=>true, 'line'=>__LINE__, 'file'=>__FILE__, 'type'=>'image_create');

		$tmpWidth  = $newWidth  = imagesx($tempImage);
		$tmpHeight = $newHeight = imagesy($tempImage);

		//resize the image
		foreach($width as $n => $vals)
		{
			$toWidth  = $width[$n];
			$toHeight = $height[$n];
			$sizeName = 'size'.strval($n+1).'_'.$newName;

			if ($tmpWidth > $tmpHeight) {
				if ($tmpWidth > $toWidth) {
					$newWidth = $toWidth;
					$newHeight = round($tmpHeight*($toWidth/$tmpWidth));
				}
			} else {
				if ($tmpHeight > $toHeight) {
					$newHeight = $toHeight;
					$newWidth = round($tmpWidth*($toHeight/$tmpHeight));
				}
			}

			//copy the image with the new values
			$newImage = imagecreatetruecolor($newWidth,$newHeight);

			//keep transparent background for png
			if($imageInfo['mime'] == 'image/png'){
				imagealphablending($newImage, false);
				imagesavealpha($newImage, true);
				imagefill($newImage, 0, 0, imagecolorallocatealpha($newImage, 255, 255, 255, 1));
			}

			//keep transparent background for gif
			if($imageInfo['mime'] == 'image/gif'){
				$transparent_index = imagecolortransparent($tempImage);
				if ($transparent_index >= 0){
				    imagepalettecopy($tempImage, $newImage);
				    imagefill($newImage, 0, 0, $transparent_index);
				    imagecolortransparent($newImage, $transparent_index);
				    imagetruecolortopalette($newImage, true, 256);
				}
			}

			//generate the new image
			imagecopyresampled($newImage,$tempImage,0,0,0,0,$newWidth,$newHeight,$tmpWidth,$tmpHeight);

			//create the file depending off the type image and atach the size
			if($imageInfo['mime'] == 'image/jpeg' || $imageInfo['mime'] == 'image/pjpeg')  if(!imagejpeg($newImage, WWW_ROOT . $path . $sizeName)){
				$return = array('error'=>true, 'type'=>'image_create');
				break;
			}

			if($imageInfo['mime'] == 'image/gif') if(!imagegif($newImage, WWW_ROOT . $path . $sizeName)){
				$return = array('error'=>true, 'type'=>'image_create');
				break;
			}

			if($imageInfo['mime'] == 'image/png') if(!imagepng($newImage, WWW_ROOT . $path . $sizeName)){
				$return = array('error'=>true, 'type'=>'image_create');
				break;
			}
		}

		//if all good save the original image
		if(isset($options['original']) && !$return['error'] && !rename($file['tmp_name'], WWW_ROOT . $path . 'original_' . $sizeName))
			$return = array('error'=>true, 'type'=>'image_original');

		//some error to the log
		if($return['error']){
			$this->log('File: '.__FILE__.' Line: '.__LINE__.' msg: uploading image type: '.$file['type'].' image dimensions '.$origX.'-'.$origY.' to '.WWW_ROOT . Configure::read('path.uploaded.images') . $newName);
			$return['type'] = 'error';
			return $return;
		}

		//Read and write for owner, read for everybody else
		@chmod(WWW_ROOT . $path . $sizeName, 0644);

		//all good return the name
		return $newName;
	}

	function checkMemory($file)
	{
		//TODO check if this work to all images in all server .. can do that ?
		$imageInfo = getimagesize($file['tmp_name']);

		if($imageInfo['mime'] == 'image/jpeg' || $imageInfo['mime'] == 'image/pjpeg')
			$needed = Round(($imageInfo[0] * $imageInfo[1] * $imageInfo['bits'] * $imageInfo['channels'] / 8 + Pow(2, 16)) * 1.65);

		if($imageInfo['mime'] == 'image/png')
			$needed = $this->convertBytes(Round(($imageInfo[0] * $imageInfo[1] * $imageInfo['bits'])));

		if($imageInfo['mime'] == 'image/gif')
			$needed = Round(($imageInfo[0] * $imageInfo[1] * $imageInfo['bits'] * $imageInfo['channels'] / 8 + Pow(2, 16)) * 0.85);

		if(!isset($needed))
			return false;

		$current = $needed+($this->convertBytes(memory_get_usage()));
		$default = $this->convertBytes(ini_get('memory_limit'));

		if($current > $default){
			return false;
		}
		return true;
	}

	/**
	 * Converts a literal filesize (ej. 32M) to integer bytes (ej. 33554432). =
	 * @param  string $value
	 * @return bytes
	 */
	private function convertBytes( $value )
	{
	    if ( is_numeric( $value ) ) {
	        return $value;
	    } else {
	        $value_length = strlen( $value );
	        $qty = substr( $value, 0, $value_length - 1 );
	        $unit = strtolower( substr( $value, $value_length - 1 ) );
	        switch ( $unit ) {
	            case 'k':
	                $qty *= 1024;
	                break;
	            case 'm':
	                $qty *= 1048576;
	                break;
	            case 'g':
	                $qty *= 1073741824;
	                break;
	        }
	        return $qty;
	    }
	}


	function resize($size, $img)
	{
		$path = Configure::read('path.uploaded.images');
		$orig = @getimagesize(WWW_ROOT.$path.$img);

		if(!$orig) return;
		//takes the larger size of the width and height and applies the
		//formula accordingly...this is so this script will work
		//dynamically with any size image

		if ($orig[0] > $orig[1]) {
			$percentage = ($size / $orig[0]);
		} else {
			$percentage = ($size / $orig[1]);
		}


		//gets the new value and applies the percentage, then rounds the value
		$width = round($orig[0] * $percentage);
		$height = round($orig[1] * $percentage);

		//returns the new sizes in html image tag format...this is so you
		//can plug this function inside an image tag and just get the
		//return "width=\"$width\" height=\"$height\"";
		return 'src="/'.$path.$img.'" width="'.$width.'" height="'.$height.'"';
	}

	function crop($size, $img)
	{
		$path = Configure::read('path.uploaded.images');
		$orig = @getimagesize(WWW_ROOT.$path.$img);

		if(!$orig) return;
		//takes the larger size of the width and height and applies the
		//formula accordingly...this is so this script will work
		//dynamically with any size image

		$dimensions = explode(',',$size);

		//fix the with
		if ($orig[0] > $orig[1]) {
			$percentage = ($dimensions[0] / $orig[0]);
		} else {
			$percentage = ($dimensions[1] / $orig[1]);
		}

		//gets the new value and applies the percentage, then rounds the value
		$width = round($orig[0] * $percentage);
		$height = round($orig[1] * $percentage);

		//check if the with or the height is bigger than the dimensions
		if($width > $dimensions[0]){
			$diff = $width-$dimensions[0];

			//scale both width and height
			$width = $width-$diff;
			$height = $height-$diff;
		}

		if($height > $dimensions[1]){
			$diff = $height-$dimensions[1];

			//scale both width and height
			$width = $width-$diff;
			$height = $height-$diff;
		}

		//returns the new sizes in html image tag format...this is so you
		//can plug this function inside an image tag and just get the
		//return "width=\"$width\" height=\"$height\"";
		return 'src="/'.$path.$img.'" width="'.$width.'" height="'.$height.'"';
	}

	//get the width and the height to one image resized
	function getXY($img,$size=-1)
	{
		$path = Configure::read('path.uploaded.images');
		$orig = @getimagesize(WWW_ROOT.$path.$img);

		if(!$orig) return;
		//takes the larger size of the width and height and applies the
		//formula accordingly...this is so this script will work
		//dynamically with any size image

		if($size==-1)
			return $orig;

		if ($orig[0] > $orig[1]) {
			$percentage = ($size / $orig[0]);
		} else {
			$percentage = ($size / $orig[1]);
		}

		return array($orig[0] * $percentage,$orig[1] * $percentage);
	}
}
?>