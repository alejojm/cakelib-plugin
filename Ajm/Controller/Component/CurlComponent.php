<?php

class CurlComponent extends Component
{
	/* resouce id */
	var $handle = null;

	/* options http://us2.php.net/manual/en/function.curl-setopt.php */
	var $options = array(
		CURLOPT_URL=>'',
		CURLOPT_HEADER=>false,
		CURLOPT_RETURNTRANSFER=>true,
		CURLOPT_FOLLOWLOCATION=>false,
		CURLOPT_USERAGENT=>'',
		CURLOPT_POST=>false,
		CURLOPT_POSTFIELDS=>'',
		CURLOPT_COOKIE=>'',
		CURLOPT_REFERER=>'',
		CURLOPT_CONNECTTIMEOUT=>10
	);

	/* nice options with their equivalent */
	var $_options = array(
		'url'=>CURLOPT_URL,
		'header'=>CURLOPT_HEADER,
		'content'=>CURLOPT_RETURNTRANSFER,
		'location'=>CURLOPT_FOLLOWLOCATION,
		'useragent'=>CURLOPT_USERAGENT,
		'post'=>CURLOPT_POST,
		'get'=>CURLOPT_HTTPGET,
		'postfields'=>CURLOPT_POSTFIELDS,
		'cookie'=>CURLOPT_COOKIE,
		'referer'=>CURLOPT_REFERER,
	);

	var $response = '';
	var $error = '';
	var $data = '';

	/* init */
	function __construct()
	{
		$this->options[CURLOPT_USERAGENT] = $_SERVER['HTTP_USER_AGENT'];
	}


	function get($url, $opt=null)
	{
		$this->response = '';
		$this->handle = curl_init();
		$this->options[CURLOPT_URL] = str_replace(' ', '%20', $url);

		//parse the options, make sure is a get header
		$opt['post'] = false;
		$opt['get'] = true;
		$this->parse($opt);

		//atach the url and send, close
		curl_setopt_array($this->handle,$this->options);//atach the options
		$this->response = curl_exec($this->handle);

		$this->getErrors($this->handle);
		$this->close();//close connection
		return $this->response;
	}

	function post($url, $opt=array())
	{
		$this->response = '';
		$this->handle = curl_init();

		//atach the url
		$this->options[CURLOPT_URL] = $url;

		//parse the options, make sure is a post header
		$opt['post'] = true;
		$opt['get'] = false;

		if(isset($opt['data']))
			$this->options[CURLOPT_POSTFIELDS] = is_array($opt['data']) ? $this->queryString($opt['data']) : $opt['data'];

		if(isset($opt['cookie']))
			$this->options[CURLOPT_COOKIE] = is_array($opt['cookie']) ? $this->formatCookie($opt['cookie']) : $opt['cookie'];

		//parse the options
		$this->parse($opt);

		//atach the url and send, close
		curl_setopt_array($this->handle, $this->options);//atach the options
		$this->response = curl_exec($this->handle);

		$this->getErrors($this->handle);
		$this->close();//close connection
		return $this->response;
	}

	private function parse($opt=null)
	{
		if(is_array($opt)) foreach($opt as $name => $value)
		{
			if(isset($this->_options[$name]))
			{
				$this->options[$this->_options[$name]] = $value;
			}
		}

		//pr($this->options);
	}

	function getErrors($handle)
	{
		$this->error = curl_error($handle).' # '.curl_errno($handle);
	}

	private function queryString($data)
	{
		$queryString = '';

		foreach ($data as $model => $fieldValues)
		{
			foreach($fieldValues as $field => $value)
			{
				$queryString .= rawurlencode('data['.$model.']['.$field.']').'='.rawurlencode($value).'&';
			}
		}

		return $queryString;
	}

	private function formatCookie()
	{

	}

	private function close()
	{
		curl_close($this->handle);
	}

}
?>