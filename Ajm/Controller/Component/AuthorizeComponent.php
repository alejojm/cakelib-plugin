<?php


/**
 * Create unique hash by user login
 *
 * @package default
 * @author Alejandro jiménez alejo.jm@gmail.com
 */
class AuthorizeComponent extends Component
{
	var $secret = '';
	var $desc  = array();
	var $components = array('Session');

	function initialize()
	{
		$this->desc = array('host'=>$_SERVER['SERVER_NAME'], 'br'=>$_SERVER['HTTP_USER_AGENT'], 'ip'=>$_SERVER['REMOTE_ADDR']);
		$this->secret = Configure::read('Security.salt');
	}

	/**
	 * save the session and make a hash
	 * @param  string $name
	 * @return true/false
	 * @author Alejo JM <alejo.jm@gmail.com> October 25, 2013
	 */
	function login($name, $value)
	{
		//write session, the name and value is only one is not hashed
		$this->Session->write($name, $value);

		//one by one
		$s['name']   = sha1($value);
		$s['host']   = sha1($this->desc['host']);
		$s['br']     = sha1($this->desc['br']);
		$s['ip']     = sha1($this->desc['ip']);
		$s['secret'] = sha1($this->secret);

		$this->Session->write('hash'.$name, sha1($s['name'].$s['host'].$s['br'].$s['ip'].$s['secret']));
	}

	/**
	 * test if the session hash is good
	 * @param  string $name
	 * @return true/false
	 * @author Alejo JM <alejo.jm@gmail.com> October 25, 2013
	 */
	function loggedIn($name)
	{
		if(!$this->Session->read('hash'.$name))
			return false;

		if(!$this->Session->read($name))
			return false;

		$sField = $this->Session->read($name);

		//one by one
		$s['name']   = sha1($sField);
		$s['host']   = sha1($this->desc['host']);
		$s['br']     = sha1($this->desc['br']);
		$s['ip']     = sha1($this->desc['ip']);
		$s['secret'] = sha1($this->secret);

		//all hash in the session
		$hash = $this->Session->read('hash'.$name);

		//they most be the same
		if($hash != sha1($s['name'].$s['host'].$s['br'].$s['ip'].$s['secret']))
			return false;

		return true;
	}

	/**
	 * read from session
	 * @param  string $name the name in the session
	 * @return mixed       whatever was saved in the session
	 * @author Alejo JM <alejo.jm@gmail.com> October 25, 2013
	 */
	function getUser($name){

		$someone = $this->Session->read($name);

		if(empty($someone))
			return false;

		return $someone;
	}

	/**
	 * clean the session
	 * @param  string $name
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> October 25, 2013
	 */
	function logout($name){

		$this->Session->delete($name);
		$this->Session->destroy();

	}

}

?>