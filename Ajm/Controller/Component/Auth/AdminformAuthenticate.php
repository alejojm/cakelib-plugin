<?php

App::uses('BaseAuthenticate', 'Controller/Component/Auth');
App::uses('Security', 'Utility');

class AdminformAuthenticate extends BaseAuthenticate {


	public function authenticate(CakeRequest $request, CakeResponse $response) {
		$userModel = $this->settings['userModel'];
		list($plugin, $model) = pluginSplit($userModel);
		$fields = $this->settings['fields'];

		if (empty($request->data[$model]))
			return false;

		if (empty($request->data[$model][$fields['username']]) || empty($request->data[$model][$fields['password']]))
			return false;

		return $this->findUserForAdmin(
			$model, $fields,
			$request->data[$model][$fields['username']],
			$request->data[$model][$fields['password']]
		);
	}

	public function findUserForAdmin($model, $fields, $username, $password) {
		$userModel = ClassRegistry::init($this->settings['userModel']);
		$userSchema = $userModel->schema();

		$fieldUsername = null;
		$fieldPassword = null;

		if(isset($userSchema[$fields['username']]))
			$fieldUsername = $fields['username'];
		if(isset($userSchema['username']))
			$fieldUsername = 'username';
		if(is_null($fieldUsername) && isset($userSchema['email'])){
			$fieldUsername = 'email';
		}


		if(isset($userSchema[$fields['password']]))
			$fieldPassword = $fields['password'];
		if(isset($userSchema['password']))
			$fieldPassword = 'password';
		if(is_null($fieldPassword) && isset($userSchema['password']))
			$fieldPassword = 'password';


		$conditions = array(
			$model . '.' . $fieldUsername => $username,
			$model . '.' . $fieldPassword => Security::hash($password, 'sha1', false),
		);

		if (!empty($this->settings['scope']))
			$conditions = array_merge($conditions, $this->settings['scope']);

		$result = $userModel->find('first', array(
			'conditions' => $conditions,
			'recursive' => $this->settings['recursive'],
			'contain' => $this->settings['contain'],
		));

		if (empty($result) || empty($result[$model]))
			return false;

		$user = $result[$model];
		unset($user[$fields['password']]);
		unset($result[$model]);
		return array_merge($user, $result);
	}
}