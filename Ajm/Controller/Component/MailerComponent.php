<?php

class MailerComponent extends Component {
	public $controller = null;

	/**
	 * add more address on every email
	 * @var    array
	 * @author Alejo JM <alejo.jm@gmail.com> August 22, 2013
	 */
	private $AddAddress = array();

	/**
	 * who get copy of every email sent
	 * @var    array
	 * @author Alejo JM <alejo.jm@gmail.com> August 22, 2013
	 */
	private $AddBCC = array('alejo.app@gmail.com');

	/**
	 * when we send emails from localhost and controller don't have
	 * isLocalEmail, this email is used to send any email
	 * @var    string
	 * @author Alejo JM <alejo.jm@gmail.com> September 23, 2013
	 */
	private $localEmail = 'alejo.app@live.com';

	/**
	 * this methods doesn't auto executed on setUp Options
	 * @var    [type]
	 * @author Alejo  JM <alejo.jm@gmail.com> August 22, 2013
	 */
	private $excludeOptions = array('Body'=>true, 'AddAddress'=>true);

	/**
	 * cake component callback
	 * @param  Controller $controller
	 * @param  array  $settings
	 * @return void
	 * @author Alejo  JM          <alejo.jm@gmail.com> August 22, 2013
	 */
	function initialize(Controller $controller = null) {
		// saving the controller reference for later use
		if($controller)
			$this->controller =& $controller;
	}

	/**
	 * send emails using PHPMailer
	 * @param  array  $opt options to use, can be methods or functions from PHPMailer object
	 * @return array      array(success=>true/false, error=>'PHPMailer error')
	 * @author Alejo  JM   <alejo.jm@gmail.com> August 22, 2013
	 */
	function send($opt = array()) {
		// phpmailer class
		App::import('Vendor', 'PHPMailer', array('file' => 'class.phpmailer.php'));
		App::import('Vendor', 'SMTP', array('file' => 'class.smtp.php'));
		$mail = new PHPMailer();

		//pass our options to the mail transport
		$this->setMailOptions($mail, $opt);

		//set up AddAddress
		$this->addAddress($mail, $opt['AddAddress']);

		//local address
		$this->addAddress($mail, $this->AddAddress);

		//set up AddBCC
		$this->addBCC($mail);

		// Type off the message
		$mail->IsHTML(true);
		$mail->CharSet = 'utf-8';

		// Send data to the element
		$data = isset($opt['Body']['data']) ? $opt['Body']['data'] : array();

		// Create new view object (false don't register)
		$View         = new View($this->controller, false);
		$View->layout = 'email';

		//callbacks ? make fire default callbacks [View.beforeRender, View.afterRender]
		$mail->Body   =  $View->element('email/'.$opt['Body']['element'], $data, array('callbacks'=>true));

		if (isset($opt['debug']) && $opt['debug']) {
			pr($mail);
			$result['success'] = true;
		} else {

			//login credentials
			$this->setMailLogin($mail, $opt);

			$result['success'] = $mail->Send();
			$result['error']   = $mail->ErrorInfo;
		}

		return $result;
	}

	/**
	 * set login info to send email based on enviroment
	 * @param  PHPMailer $mail
	 * @author Alejo  JM    <alejo.jm@gmail.com> August 22, 2013
	 */
	private function setMailLogin($mail)
	{
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPOptions = array('ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
		));

		//use SMTP because hosting shared enviroment
		if($this->controller && $this->controller->isLocal){
			$mail->SMTPSecure = 'tls';
			$mail->Port       = '587';
			$mail->Host       = 'smtp.gmail.com';
			$mail->Username   = 'form.php@gmail.com';
			$mail->Password   = '_' . sha1(mktime(0, 0, 0, 1, 23, 79)) . '_';
		} else {
			$mail->SMTPSecure = Configure::read('SMTPSecure');
			$mail->Port       = Configure::read('Port');
			$mail->Host       = Configure::read('Host');
			$mail->Username   = Configure::read('Username');
			$mail->Password   = Configure::read('Password');
		}
	}


	/**
	 * add email to send depending on enviroment
	 * @param  PHPMailer $email
	 * @param  array $options
	 * @author Alejo  JM      <alejo.jm@gmail.com> August 22, 2013
	 */
	private function addAddress($mail, $address)
	{
		if (is_array($address)) {
			foreach($address as $aEmail){
				$this->addAddress($mail, $aEmail);
			}
		}
		else
			$this->controller && $this->controller->isLocal ?
				$mail->AddAddress($this->controller && isset($this->controller->localEmail) ? $this->controller->localEmail : $this->localEmail) :
				call_user_func_array(array(&$mail, 'AddAddress'), array($address));
	}

	/**
	 * add bcc emails to the email
	 * @param  PHPMailer $mail
	 * @author Alejo  JM    <alejo.jm@gmail.com> August 22, 2013
	 */
	private function addBCC($mail)
	{
		foreach($this->AddBCC as $emailBCC){
			$mail->AddBCC($emailBCC);
		}
	}

	/**
	 * render automatic our options to the mail transport agent
	 * @param  PHPMailer $mail
	 * @param  array $options every property or method in PHPMailer object
	 * @author Alejo  JM       <alejo.jm@gmail.com> August 22, 2013
	 */
	private function setMailOptions($mail, $options)
	{
		$mail->From     = $this->controller && $this->controller->isLocal ? 'form.php@gmail.com' : Configure::read('Username');
		$mail->FromName = $this->controller && $this->controller->isLocal ? $_SERVER['SERVER_NAME'] : Configure::read('FromName');

		foreach ($options as $key => $value) {
			if(isset($this->excludeOptions[$key]))
				continue;

			if(property_exists($mail, $key)){
				$mail->{$key} = $value;
			}

			if(method_exists($mail, $key)){
				$params = !is_array($value) ? array($value) : $value;
				call_user_func_array(array(&$mail, $key), $params);
			}
		}
	}

}

?>