<?php
/**
 * Application Lib Controller
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 */
class AjmAppController extends Controller {

	/**
	 * local ips used for testing stuff or Configure::write('localIps', array('127.0.0.1'))
	 * @var    array
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	protected $localIps = array('127.0.0.1'=>true, '::1'=>true);

	/**
	 * localDomains used for testing stuff or Configure::write('localDomains', array('example.com'))
	 * @var    array
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	protected $localDomains = array('localhost'=>true);

	/**
	 * email for local host environments
	 * @var    string
	 * @author Alejo  JM <alejo.jm@gmail.com> August 16, 2013
	 */
	protected $localEmail = 'alejo.app@live.com';

	/**
	 * the proyect isLocal or is real life
	 * @var    boolean
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	public $isLocal = false;

	/**
	 * nice shortcuts, inside controllers and views
	 * @var    array
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	protected $_servervalues = array(
		'host'             => '$_SERVER["SERVER_NAME"]',
		'ip'               => '$_SERVER["REMOTE_ADDR"]',
		'cookie'           => '$_SERVER["HTTP_COOKIE"]',
		'referer'          => '$_SERVER["HTTP_REFERER"]',
		'agent'            => '$_SERVER["HTTP_USER_AGENT"]',
		'qs'               => '$_SERVER["QUERY_STRING"]',
		'method'           => '$_SERVER["REQUEST_METHOD"]',
		'forwarded_for'    => '$_SERVER["HTTP_X_FORWARDED_FOR"]',
		'forwarded_host'   => '$_SERVER["HTTP_X_FORWARDED_HOST"]',
		'forwarded_server' => '$_SERVER["HTTP_X_FORWARDED_SERVER"]',
		'proto'            => 'isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "https://" : "http://"',
		'url'              => '$this->proto.$this->host.$this->webroot'
	);

	/**
	 * Add the testing controller if exits
	 *
	 * @return void
	 * @author Alejo jm
	 */
	public function beforeFilter(){
		//parse shortcuts
		$this->_servervalues();

		//set environment based in localDomains and localIps
		$this->setEnvironment();

		//we need facebook api
		if(is_file(APP.'Model'.DS.'FacebookConfig.php')){
			$this->loadModel('FacebookConfig');
			$this->FacebookConfig->setIsLocal($this->isLocal);
		}

		//clean all data to the app
		App::uses('Sanitize', 'Utility');
		$this->request->data   = Sanitize::clean($this->request->data, array('encode' => false, 'carriage'=>false, 'escape'=>false));
		$this->request->query  = Sanitize::clean($this->request->query, array('encode' => false, 'carriage'=>false, 'escape'=>false));
		$this->request->params['named'] = Sanitize::clean($this->request->params['named'], array('encode' => false, 'carriage'=>false, 'escape'=>false));
		$this->request->params['pass'] = Sanitize::clean($this->request->params['pass'], array('encode' => false, 'carriage'=>false, 'escape'=>false));

		//load testing controller
		$this->loadTestingController();

		if ($this->name == 'CakeError' && !Configure::read('debug')) {
  			$this->layout = 'Ajm.404';
    	}
	}

	/**
	 * test if we are in local deploy
	 * @author Alejo JM <alejo.jm@gmail.com> October 10, 2013
	 */
	protected function setEnvironment()
	{
		if(Configure::read('localDomains'))
			$this->localDomains = array_merge($this->localDomains, (array)Configure::read('localDomains'));

		if(Configure::read('localIps'))
			$this->localIps = array_merge($this->localIps, (array)Configure::read('localIps'));

		if(Configure::read('localEmail'))
			$this->localEmail = Configure::read('localEmail');

		//local enviroment by ip
		$localIp = isset($this->localIps[$this->ip]) && $this->localIps[$_SERVER['REMOTE_ADDR']];

		//local enviroment by forwared_host "apache proxy"
		$localForward = isset($this->localDomains[$this->forwarded_host]) && $this->localDomains[$this->forwarded_host];

		//local enviroment by host
		$localDomain = isset($this->localDomains[$this->host]) && $this->localDomains[$this->host];

		if($localIp || $localForward || $localDomain)
			$this->isLocal = true;

	}


	/**
	 * load testing controller if we are in development
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> August 23, 2013
	 */
	protected function loadTestingController()
	{
		//check if we have testing controller in deployment mode
		if(Configure::read('debug')){
			$className = $this->name.'TestingController';
			$pathFileName = APP.'Controller'. DS .'Testing'. DS .$className.'.php';

			if(is_file($pathFileName)){
				if(!class_exists($className))
					require $pathFileName;

				$this->Testing = new $className();
				$this->Testing->controller = $this;
				if(method_exists($this->Testing, $this->action)){
					$this->Testing->{$this->action}();
				}

			}
		}

	}

	/**
	 * (override) add the event View.beforeRender, we need add extra vars to the view
	 * @return void
	 */
	public function implementedEvents(){
		$events = parent::implementedEvents();
		$events['View.beforeRender'] = 'viewBeforeRender';
		return $events;
	}

	/**
	 * (override) render the view based on enviroment
	 * @param  string $view
	 * @param  string $layout
	 * @return string
	 * @author Alejo JM <alejo.jm@gmail.com> October 02, 2013
	 */
	public function render($view = null, $layout = null) {
		//cakeError and debug, show the cake error
		if ($this->name == 'CakeError' && Configure::read('debug')){
			$this->layout = 'default';
			return parent::render($view, $layout);
		}

		$pluginDir = '';
		//is a plugin add the path
		if(!empty($this->params['plugin'])){
			$pluginDir = 'Plugin' . DS . Inflector::camelize($this->params['plugin']) . DS;
		}

		//view exists or use empty
		if(!$view){
			if(is_file(APP . $pluginDir. 'View' . DS . $this->name . DS . $this->view . $this->ext)){
				$view = $this->view;
			}	else	{
				$view = '/empty';
			}
		}

		if(!$view && $this->layout == 'ajax'){
			$view = '/empty';
		}

		return parent::render($view, $layout);
	}

	/**
	 * process _servervalues add everyone to the controller
	 * @return [type] void
	 * @author Alejo JM <alejo.jm@gmail.com>
	 */
	protected function _servervalues($scope=false){
		foreach ($this->_servervalues as $name => $value)
		{
			$eval = @eval('return @'.$value.';');
			$applyTo = !$scope ? $this : $scope;
			$applyTo->$name = $eval;
			$applyTo->isLocal = $this->isLocal;
		}
		//remove las slash from url var
		$applyTo->url = substr($applyTo->url, 0, strlen($applyTo->url)-1);
	}


	/**
	 * Add to the view, my own vars
	 * @param  CakeEvent $e
	 * @return void
	 */
	public function viewBeforeRender($e){
		$this->_servervalues($e->subject);
	}

	/**
	 * test if the data have specific keys
	 * @param  string  $model  ModelName
	 * @param  string  $fields field1,field2,field3
	 * @return boolean true/false
	 * @author Alejo   JM      <alejo.jm@gmail.com> August 21, 2013
	 */
	public function dataHasKeys($model='', $fields='')
	{
		if(empty($this->data) || !is_array($this->data))
			return false;

		if(!empty($model) && !isset($this->data[$model]))
			return false;

		$replaced = str_replace(' ', '', $fields);
		$fields   = explode(',', $replaced);
		$found    = true;

		foreach ($fields as $n => $field) {
			if(!empty($model))
				$found = (isset($this->data[$model]) && isset($this->data[$model][$field]));
			else
				$found =  isset($this->data[$field]);

			if(!$found)
				return false;
		}

		return $found;
	}

	/**
	 * php 5.2 don't have json predefined constant see: http://www.php.net/manual/en/json.constants.php
	 * so this method make a base64_encode json to pass to the javascript world safely, in javascript
	 * must remove the ' character like : var object = jsonString.replace(/'/g, '');
	 * @deprecated  Doesn't work in all servers (DONT USE)
	 * @param  array  $info
	 * @return string       base64+json+"'"
	 * @author Alejo JM <alejo.jm@gmail.com> October 15, 2013
	 */
	public function base64Json($info = array()){
		$json = json_encode($info);
		$json = str_replace(':"', ':"'."'", $json);
		$json = str_replace('",', "'".'",', $json);
		$json = str_replace('"}', "'".'"}', $json);
		return  base64_encode($json);
	}

	/**
	 * Convert array to key:value;
	 * @param  array  $info
	 * @return string
	 * @author Alejo JM <alejo.jm@gmail.com> October 31, 2013
	 */
	public function urlColonSemicolon($info=array())
	{
		$string = '';
		foreach ($info as $key => $value) {
			$string .= $key.':'.$value.';';
		}
		$string = substr($string, 0, (strlen($string)-1));
		return $string;
	}

}

