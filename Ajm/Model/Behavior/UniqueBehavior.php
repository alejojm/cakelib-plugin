<?php


class UniqueBehavior extends ModelBehavior {

	/**
	 * cake callback find if is the same data for remove the validation
	 * @param  Model  $model
	 * @param  array  $options
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> April 14, 2015
	 */
	public function beforeValidate(Model $model, $options = array()) {
		$schema = $model->schema();
		foreach ($schema as $name => $field) {
			if(isset($field['key']) && $field['key']=='unique' && isset($model->data[$model->name][$name])){
				$info = $model->{'findBy'.$name}($model->data[$model->name][$name], $name);
				// the row exists and the data es the same remove the validation
				if(isset($info[$model->name][$name]) && $info[$model->name][$name] == $model->data[$model->name][$name]){
					if(isset($model->validate[$name]) && isset($model->validate[$name]['isUnique'])){
						// ajmdebugger::pr('aca');
						unset($model->validate[$name]['isUnique']);
					}

				}

			}
		}

		return true;
	}
}