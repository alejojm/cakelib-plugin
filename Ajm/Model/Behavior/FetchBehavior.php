<?php
/**
 * keep all your cake relations working the only difference is that the FetchBehavior will generate only one query with
 * for all your relations. the resultSet will have only one depth for every relation.
 * Plus... you can generate csv from mysql directly.
 */
class FetchBehavior extends ModelBehavior {
	/**
	 * Model dataSource (Mysql)
	 * @var Mysql
	 */
	protected $db = null;

	/**
	 * the active model
	 * @var string
	 */
	protected $currentModel = '';

	/**
	 * the joins for models only add new one if doesn't exists here
	 * @var array
	 */
	protected $definedJoins = array();

	/**
	 * the joins for models only add new one if doesn't exists here
	 * @var array
	 */
	protected $recursiveCount = 0;


	/**
	 * all processed association
	 * @var array
	 */
	protected $mapAssociations = array();

	/**
	 * setup the dataSource
	 */
	public function setup(Model $Model, $config = array()) {
		$this->db = $Model->getDataSource();
		$this->currentModel = $Model->alias;
	}

	/**
	 * keep working the count from the controller, cake will call this function magically
	 * @return array
	 */
	public function paginateCount(Model $Model, $conditions, $recursive, $extra)
	{
		$options = array_merge(array('conditions'=>$conditions), $extra);
		return $this->fetch($Model, 'count', $options);
	}

	/**
	 * keep working pagination from the controller, cake will call this function magically
	 * @return array
	 */
	public function paginate(Model $Model, $conditions, $fields, $order, $limit, $page, $recursive, $extra)
	{
		$options = array_merge(array(
			'conditions'=>$conditions,
			'fields'=>$fields,
			'order'=>$order,
			'limit'=>$limit,
			'offset'=>$page > 1 && !empty($limit) ? ($page - 1) * $limit : 0,
			'page' => $page
		), $extra);

		return $this->fetch($Model, 'all', $options);
		//TODO maybe we can return the array like cake does, need better array access
		// return $this->filterResults($Model, $resultSet);
	}

	/**
	 * reset properties for new query
	 * @return void
	 */
	protected function reset()
	{
		$this->definedJoins = array();
		$this->mapAssociations = array();
		$this->recursiveCount = 0;
	}

	/**
	 * fetch result from database
	 * @param  Model  $Model
	 * @param  array $options
	 * @return array
	 */
	public function fetch(Model $Model, $type='all', $options=array()){
		$this->reset();

		if($type == 'first')
			$options['limit'] = 1;

		if($type == 'count'){
			$options['fields'] = $this->db->name('COUNT('.$Model->alias.'.'.$Model->primaryKey.') AS count');
		}

		$isFile = $type == 'file';
		$query = $this->buildQuery($Model, $type, $options, $isFile);
		$beforeFind = $Model->beforeFind($query);
		if(!$beforeFind)
			return null;
		$query = $beforeFind && is_bool($beforeFind) ? $query : $beforeFind;
		$statement = $this->buildStatement($Model, $query);
		if($isFile){
			return $this->addFileStament($Model, $statement, $query);
		}

		$resultSet = $this->db->fetchAll($statement, $Model->cacheQueries);
		$resultSet = $Model->afterFind($resultSet);

		if($type == 'count'){
			return count($resultSet) == 1 && isset($resultSet[0][0]['count']) ? $resultSet[0][0]['count'] : count($resultSet);
		}

		if($type == 'first' && isset($resultSet[0]))
			return !empty($resultSet[0][$Model->alias][$Model->primaryKey]) ? $resultSet[0] : array();

		return $resultSet;
	}

	/**
	 * create csv for mysql sql query
	 * @param  Model  $Model
	 * @param  array $options normal cake find options
	 * @return void
	 */
	protected function addFileStament(Model $Model, $statement, $query)
	{
		$sqlFieldsNames = $this->translate($query['fields']);

		//save the query result to one file
		$os_end     = strpos($_SERVER['HTTP_USER_AGENT'], 'indows') !== false ? ';' : ',';
		$fileName   = isset($query['file']['name']) ? $query['file']['name'] : $Model->alias.'_'.time().'.csv';
		$path       = isset($query['file']['path']) ? $query['file']['path'] : '/tmp/';
		$fields_end = isset($query['file']['fields_end']) ? $query['file']['fields_end'] : $os_end;
		$enclosed   = isset($query['file']['enclosed']) ? $query['file']['enclosed'] : '"';
		$lines_end  = isset($query['file']['lines_end']) ? $query['file']['lines_end'] : '\n';

		$filePath = $path.$fileName;
		$statement .= " INTO OUTFILE '{$filePath}' FIELDS TERMINATED BY '{$fields_end}' ENCLOSED BY '{$enclosed}' LINES TERMINATED BY '{$lines_end}' ";
		$sqlFieldNames = "SELECT $sqlFieldsNames UNION ALL ".$statement;

		if($this->db->execute($sqlFieldNames))
			return $filePath;
		else
			return false;
	}


	/**
	 * build sql stament for given cake options
	 * @param  Model  $Model
	 * @param  array $options
	 * @param  bool $orderFields
	 * @return array
	 */
	protected function buildQuery(Model $Model, $type='all', $options, $orderFields=false)
	{
		$this->definedJoins[$this->currentModel] = true;
		if($type == 'count'){
			$fields = $options['fields'];
		}
		else
		{
			$fields[$Model->alias] = $this->db->fields($Model, $Model->alias, @$options['fields']);
			$this->checkFields($Model, $fields[$Model->alias]);
		}

		$joins = array();
		$contains = $this->getContains($Model, $options);
		if($Model->recursive !== -1)
			$this->getRelations($Model, $options, $fields, $joins, $contains);

		if($type != 'count')
			$fields = array_unique(call_user_func_array('array_merge', array_values($fields)));

		if($orderFields && isset($options['fields']))
			$this->keepUserOrderFields($options['fields'], $fields);

		return array(
			'fields'=>$fields,
			'joins'=>$joins,
			'conditions' => @$options['conditions'],
			'fields' => $fields,
			'order' => @$options['order'],
			'limit' => @$options['limit'],
			'joins' => @$joins,
			'group' => @$options['group'],
			'file' => @$options['file'],
		);
	}

	/**
	 * generate sql statment
	 * @param  Model  $Model
	 * @param  array $query
	 * @return string
	 */
	protected function buildStatement(Model $Model, $query)
	{
		return $this->db->renderStatement('select', array(
			'conditions' => $this->db->conditions(@$query['conditions'], true, true, $Model),
			'fields' => is_array($query['fields']) ? implode(', ', @$query['fields']) : @$query['fields'],
			'table' => $this->db->name($Model->useTable),
			'alias' => $this->db->name($Model->alias),
			'order' => $this->db->order(@$query['order'], 'ASC', $Model),
			'limit' => $this->db->limit(@$query['limit'], @$query['offset']),
			'joins' => implode(' ', @$query['joins']),
			'group' => $this->db->group(@$query['group'], $Model)
		));
	}

	/**
	 * get joins model recursive
	 * @param  Model $Model
	 * @param  array $options
	 * @param  array $fields
	 * @param  array $joins
	 * @return void
	 */
	protected function getRelations(Model $Model, $options, &$fields, &$joins, $contains)
	{
		$associations = $Model->getAssociated();
		foreach ($associations as $modelName => $asso) {
			if(isset($this->definedJoins[$modelName]) || $this->currentModel == $modelName){
				continue;
			}
			if(!empty($contains) && !isset($contains[$modelName])){
				continue;
			}

			$currentAssoc = $Model->{$asso}[$modelName];
			$linkModel = ClassRegistry::init($modelName);
			$tableName = Inflector::tableize($modelName);
			$joinType = $asso == 'hasOne' ? 'INNER' : 'LEFT';
			$schemas = array('link'=>$linkModel->schema(), 'model'=>$Model->schema());
			$on = '%s.%s=%s.%s';

			if(isset($schemas['model'][$currentAssoc['foreignKey']]) && isset($schemas['link'][$linkModel->primaryKey])){
				$conditions = sprintf($on, $Model->alias, $currentAssoc['foreignKey'], $linkModel->alias, $linkModel->primaryKey);
			}

			if(isset($schemas['link'][$currentAssoc['foreignKey']]) && isset($schemas['model'][$Model->primaryKey])){
				$conditions = sprintf($on, $linkModel->alias, $currentAssoc['foreignKey'], $Model->alias, $Model->primaryKey);
			}

			if(!isset($conditions)){
				continue;
			}

			$data = array(
				'type'=>isset($currentAssoc['type']) ? $currentAssoc['type'] : $joinType,
				'table'=>$this->db->name($tableName),
				'alias'=>$this->db->name($linkModel->alias),
				'conditions'=>$this->_quoteFields($conditions),
			);

			array_push($joins, $this->db->renderJoinStatement($data));

			//count query setup always the fields
			if(isset($options['fields']) && is_string($options['fields']))
				$fields = @$options['fields'];
			else
			{
				$fields[$linkModel->alias] = $this->db->fields($linkModel, $linkModel->alias, @$options['fields']);
				$this->checkFields($linkModel, $fields[$linkModel->alias]);
			}

			$this->definedJoins[$modelName] = true;
			$this->mapAssociations[$Model->alias][$linkModel->alias] = $asso;
			if($Model->recursive == $this->recursiveCount && empty($contains))
				return;

			// $this->getRelations($linkModel, $options, $fields, $joins, $this->getContains($linkModel, $options));
			$this->getRelations($linkModel, $options, $fields, $joins, $contains);
			$this->recursiveCount++;
		}

	}


	/**
	 * translate field names using cake, don't need nasty array with header fields
	 * @param  array $fields
	 * @return string
	 */
	public function translate($fields)
	{
		$strFieldsNames = '';
		foreach ($fields as $n => $field) {
			$haveAs = strpos($field, 'AS');
			if($haveAs !== false){
				$nameField = trim(str_replace(array('AS', '`.`','`'), array('','_',''), substr($field, $haveAs, strlen($field))));
			} else {
				$nameField = str_replace(array('`.`','`'), array('_',''), $field);
			}

			$strFieldsNames .= '"'.__($nameField).'", ';
		}

		return rtrim($strFieldsNames, ", ");
	}

	/**
	 * parse contain as array or string, check if isset in Containable Behavior
	 * @param  array $options
	 * @return array
	 */
	protected function getContains(Model $Model, $options)
	{
		if($Model->Behaviors->Containable && isset($Model->Behaviors->Containable->runtime[$Model->alias]['contain']) && !isset($options['contain']))
			$options['contain'] = $Model->Behaviors->Containable->runtime[$Model->alias]['contain'];

		//the user never executed contain method use the association directly
		if(!isset($options['contain']) || !is_array($options['contain'])){
			return $Model->getAssociated();
		}
		//put at first the currentModel
		array_unshift($options['contain'], $this->currentModel);
		$contains = Hash::flatten($options['contain']);
		$allKeys = array();
		$found = array();
		foreach ($contains as $key => $contain) {
			if(is_string($contain) && strpos($key, '.') == false){
				$found = array_flip(explode('.', $contain));
			}
			if(is_string($contain) && strpos($key, '.') !== false)
			{
				$found = array_flip(explode('.', $key));
				$found[$contain] = true;
			}
			$allKeys = array_merge($allKeys, $found);
		}

		return $allKeys;
	}

	/**
	 * check for given Model all stack fields exists in schema or is virtual field
	 * @param  Model $Model
	 * @param  array $fields
	 * @return void
	 */
	protected function checkFields(Model $Model, &$fields)
	{
		foreach ($fields as $n => $field) {
			$haveAs = strpos($field, 'AS');
			if(strrpos($field, $Model->alias) !== false){
				// pr($Model->alias.' - '.$field);
				$fieldName = str_replace(array($Model->alias, '`', '.'), array('', '', ''), $field);
				if($haveAs)
					$fieldName = substr($fieldName, (strrpos($fieldName, '__')+2), strlen($fieldName));

				if(!$Model->isVirtualField($fieldName) && !$Model->schema($fieldName)){
					unset($fields[$n]);
				}

			}
		}
	}

	/**
	 * keep the original order of the array user input fields
	 * @param  array $userFieldsOrder
	 * @param  array $fields
	 * @return void
	 */
	protected function keepUserOrderFields($userFieldsOrder, &$fields)
	{
		$keepUserOrder = array();
		foreach ($userFieldsOrder as $key => $userField) {
			$userFieldName = $this->db->name($userField);
			foreach ($fields as $n => $field) {
				$haveAs = strpos($field, 'AS');
				if($field == $userFieldName){
					$keepUserOrder[$key] = $field;
					continue;
				}

				//virtual fields
				if($haveAs && $userField == str_replace('`', '', substr($field, (strrpos($field, '__')+2)))){
					$keepUserOrder[$key] = $field;
					continue;
				}
			}
		}
		$fields = $keepUserOrder;
	}

	/**
	 * TODO doesnt work
	 * nasty function, keep the resultSet array like cake do (is not the same)
	 * @param  Model  $Model
	 * @param  array $resultSet
	 * @return array
	 */
	protected function filterResults(Model $Model, $resultSet)
	{
		//no association
		if(empty($this->mapAssociations))
			return $resultSet;

		$filterResults = array();
		$masterID = null;
		$needArray = array();

		//first round keep all duplicated data into independents array
		foreach ($this->mapAssociations as $modelName => $links) {
			foreach ($resultSet as $key => $row) {
				if(empty($masterID))
					$masterID = $row[$Model->alias][$Model->primaryKey];
				else {
					if($masterID !== $row[$Model->alias][$Model->primaryKey])
						$masterID = $row[$Model->alias][$Model->primaryKey];
				}

				//belongsTo only need to be inserted
				if(!isset($filterResults[$masterID][$modelName])){
					$filterResults[$masterID][$modelName] = $row[$modelName];
				}
				//kind of hasMany or hasAndBelongsToMany
				elseif (isset($filterResults[$masterID][$modelName]) &&
						is_string(key($filterResults[$masterID][$modelName])) &&
						isset($needArray[$modelName]))
				{
					$currentRow = $filterResults[$masterID][$modelName];
					unset($filterResults[$masterID][$modelName]);
					$filterResults[$masterID][$modelName][] = $currentRow;
				}
				//the row exists setup as array
				else if( is_string(key($filterResults[$masterID][$modelName])) &&
						 $filterResults[$masterID][$modelName] !== $row[$modelName])
				{
					$needArray[$modelName] = true;
					$currentRow = $filterResults[$masterID][$modelName];
					unset($filterResults[$masterID][$modelName]);
					$filterResults[$masterID][$modelName][] = $currentRow;
					$filterResults[$masterID][$modelName][] = $row[$modelName];
				}
			}
		}

		//find rows supposed to be array with key int
		foreach ($filterResults as $key => $row) {
			foreach ($needArray as $existsName => $existsValue) {
				if(isset($row[$existsName]) && is_string(key($row[$existsName]))){
					$currentRow = $row[$existsName];
					unset($filterResults[$key][$existsName]);
					$filterResults[$key][$existsName][] = $currentRow;
				}
			}
		}


		//the first level of array, the model executing the query
		foreach ($this->mapAssociations[$Model->alias] as $linkModel => $asso)
		foreach ($resultSet as $key => $row) {
			$masterID = $row[$Model->alias][$Model->primaryKey];
			if( in_array($asso, array('belongsTo', 'hasOne')) &&
				!isset($filterResults[$masterID][$linkModel])){
				$filterResults[$masterID][$linkModel] = $row[$linkModel];
			}
		}

		//second round find linkModels in associations
		$lastDeepFound = array();
		unset($this->mapAssociations[$Model->alias]);
		foreach ($this->mapAssociations as $modelName => $links)
		foreach ($links as $linkModel => $asso)
		foreach ($resultSet as $key => $row) {
			$masterID = $row[$Model->alias][$Model->primaryKey];
			if( in_array($asso, array('belongsTo', 'hasOne')) &&
				isset($filterResults[$masterID][$modelName]) &&
				isset($row[$linkModel])
				){
				//belongsTo, hasOne must have string
				if(is_string(key($filterResults[$masterID][$modelName]))){
					$filterResults[$masterID][$modelName][$linkModel] = $row[$linkModel];
				}
				//kind off many results
				else
				{
					foreach ($filterResults[$masterID][$modelName] as $keyMany => $oneRow) {
						if(!isset($oneRow[$linkModel]) && $lastDeepFound !== $row[$linkModel]){
							$filterResults[$masterID][$modelName][$keyMany][$linkModel] = $row[$linkModel];
							$lastDeepFound = $row[$linkModel];
							break;
						}
					}
				}
			}
		}

		$this->orderFilteredResults($filterResults);
		return array_values($filterResults);
	}

	/**
	 * move arrays
	 * @param  array $filterResults
	 * @return void
	 */
	protected function orderFilteredResults(&$filterResults)
	{
		foreach ($this->mapAssociations as $modelName => $links)
		foreach ($links as $linkModel => $asso)
		foreach ($filterResults as $key => $row) {
			if(in_array($asso, array('hasMany', 'hasAndBelongsToMany'))){
				if(!isset($row[$modelName][$linkModel]) && isset($row[$linkModel])){
					$manyAsso = $filterResults[$key][$linkModel];
					unset($filterResults[$key][$linkModel]);
					$filterResults[$key][$modelName][$linkModel] = $manyAsso;
				}
			}
		}
	}


/**
 * (copy and paste from DboSource is protected we need this function)
 * Quotes Model.fields
 *
 * @param string $conditions The conditions to quote.
 * @return string or false if no match
 */
	protected function _quoteFields($conditions) {
		$start = $end = null;
		$original = $conditions;

		if (!empty($this->startQuote)) {
			$start = preg_quote($this->startQuote);
		}
		if (!empty($this->endQuote)) {
			$end = preg_quote($this->endQuote);
		}
		$conditions = str_replace(array($start, $end), '', $conditions);
		$conditions = preg_replace_callback(
			'/(?:[\'\"][^\'\"\\\]*(?:\\\.[^\'\"\\\]*)*[\'\"])|([a-z0-9_][a-z0-9\\-_]*\\.[a-z0-9_][a-z0-9_\\-]*)/i',
			array(&$this, '_quoteMatchedField'),
			$conditions
		);
		if ($conditions !== null) {
			return $conditions;
		}
		return $original;
	}


/**
 * (copy and paste from DboSource is protected we need this function)
 * Auxiliary function to quote matches `Model.fields` from a preg_replace_callback call
 *
 * @param string $match matched string
 * @return string quoted string
 */
	protected function _quoteMatchedField($match) {
		if (is_numeric($match[0])) {
			return $match[0];
		}
		return $this->db->name($match[0]);
	}

}

