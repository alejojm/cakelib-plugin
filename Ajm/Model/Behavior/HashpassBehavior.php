<?php

class HashpassBehavior extends ModelBehavior {

	/**
	 * hash password if field exists
	 * @param  Model &$model
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> January 08, 2015
	 */
	public function beforeSave(Model $model, $options = array()) {

		$hash = isset($model->actsAs['Hashpass']['hash']) ? $model->actsAs['Hashpass']['hash'] : 'sha1';

		if(isset($model->data[$model->name]['password']))
			if(!empty($model->data[$model->name]['password']))
				$model->data[$model->name]['password'] = $hash($model->data[$model->name]['password']);
			else
				unset($model->data[$model->name]['password']);

	}
}