<?php
/**
 * generate automatically validations rules when database have NOT NULL
 * @param  array  $options
 * @return void
 * @author Alejo JM <alejo.jm@gmail.com> December 16, 2014
 */
class DatabaseValidationBehavior extends ModelBehavior {

	/**
	 * add the validations
	 * @param  Model  $model
	 * @param  array  $config
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> April 14, 2015
	 */
	public function setup(Model $model, $config = array()) {
		$this->addValidation($model);
	}

	/**
	 * add validate and merge with model property validate
	 * @param  Model $model
	 * @author Alejo JM <alejo.jm@gmail.com> April 13, 2015
	 */
	protected function addValidation(Model $model){
		$validate = array();
		$schema = $model->schema();
		$validate = array();

		foreach ($schema as $name => $field) {
			if($field['null'] || @$field['key'] == 'primary' || in_array($name, array('created', 'updated')))
				continue;

			$rule = null;

			if(method_exists('Validation', $field['type'])){
				$rule = $field['type'];
				$validate[$name][$rule]['message'] = __d('cake', 'Please enter a valid '.$rule);
			}

			else if(method_exists('Validation', $name)){
				$rule = $name;
				$validate[$name][$rule]['message'] = __d('cake', 'Please enter a valid '.$rule);
			}
			else
			{
				if(in_array($field['type'], array('string', 'text'))){
					$rule = 'notEmpty';
					$validate[$name][$rule]['message'] = __d('cake', 'This field cannot be left blank');
				}

				if(in_array($field['type'], array('integer', 'biginteger', 'float'))){
					$rule = 'numeric';
					$validate[$name][$rule]['message'] = __d('cake', 'Please enter a numeric value');
				}

			}

			//have email ?
			if(!isset($validate[$name]['email']) && strpos($name, 'email') !== false){
				$validate[$name]['email']['rule'] = 'email';
				$validate[$name]['email']['required'] = true;
				$validate[$name]['email']['allowEmpty'] = false;
				$validate[$name]['email']['message'] = __d('cake', 'Please enter a valid email');
			}

			if(!$rule)
				continue;

			//general validations
			$validate[$name][$rule]['rule'] = $rule;
			$validate[$name][$rule]['required'] = true;
			$validate[$name][$rule]['allowEmpty'] = false;

			//length validation
			if(isset($field['length']) && !empty($field['length'])){
				$validate[$name]['maxLength']['rule'] = ['maxLength', $field['length']];
				$validate[$name]['maxLength']['required'] = true;
				$validate[$name]['maxLength']['allowEmpty'] = false;
				$validate[$name]['maxLength']['message'] = __d('cake', "$name must be no larger than {$field['length']} characters long.");
			}

			//unique validation
			if(isset($field['key']) && $field['key']=='unique'){
				$validate[$name]['isUnique']['rule'] = 'isUnique';
				$validate[$name]['isUnique']['required'] = true;
				$validate[$name]['isUnique']['allowEmpty'] = false;
				$validate[$name]['isUnique']['message'] = __d('cake', 'The record already exists, try another');
				// $validate[$name]['isUnique']['on'] = 'create';
			}

			//special inputs name
			if($name == 'username'){
				$validate[$name]['alphanumeric']['rule'] = 'alphanumeric';
				$validate[$name]['alphanumeric']['required'] = true;
				$validate[$name]['alphanumeric']['allowEmpty'] = false;
			}

			//special inputs name
			if($name == 'password'){
				$validate[$name][$rule]['allowEmpty'] = false;
				$validate[$name][$rule]['required'] = true;
				$validate[$name][$rule]['on'] = 'create';
			}


		}//endforeach

		//merge the generated and model property the user maybe add validations
		$model->validate = array_merge($validate, $model->validate);

	}

}
