<?php

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AjmAppModel extends Model {

	/**
	 * generate automatically validations rules when database have NOT NULL
	 * @param  array  $options
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> December 16, 2014
	 */
	public function beforeValidate($options = array()) {
		$validate = array();
		$schema = $this->schema();

		foreach ($schema as $name => $field) {
			if($field['null'] || @$field['key'] == 'primary')
				continue;

			$rule = null;

			if(method_exists('Validation', $field['type']))
				$rule = $field['type'];
			else
			{
				if(in_array($field['type'], array('string', 'text')))
					$rule = 'notEmpty';
				if(in_array($field['type'], array('integer', 'biginteger', 'float')))
					$rule = 'numeric';
			}

			if(!$rule)
				continue;

			//general validations
			$this->validate[$name][$rule]['rule'] = $rule;
			$this->validate[$name][$rule]['required'] = true;
			$this->validate[$name][$rule]['allowEmpty'] = false;

			//unique validation
			if(isset($field['key']) && $field['key']=='unique'){
				$this->validate[$name]['isUnique']['rule'] = 'isUnique';
				$this->validate[$name]['isUnique']['required'] = true;
				$this->validate[$name]['isUnique']['allowEmpty'] = false;
				$this->validate[$name]['isUnique']['message'] = 'notUnique';
			}

			//special inputs name
			if($name == 'username'){
				$this->validate[$name]['alphanumeric']['rule'] = 'alphanumeric';
				$this->validate[$name]['alphanumeric']['required'] = true;
				$this->validate[$name]['alphanumeric']['allowEmpty'] = false;
			}
		}

	}

}
