<?php
App::uses('AppModel', 'Model');
/**
 * Menu Model
 *
 */
class Menu extends AppModel {
	public $actsAs=array('Containable');
	public $useTable = 'aros_acos';
	/**
	 * reference to Acl cake php
	 * @var    [type]
	 * @author Alejo  JM <alejo.jm@gmail.com> July 30, 2013
	 */
	public $Acl;

	/**
	 * reference to login user
	 * @var    [type]
	 * @author Alejo  JM <alejo.jm@gmail.com> July 30, 2013
	 */
	public $login_info;

	/**
	 * reference controller name
	 * @var    [type]
	 * @author Alejo  JM <alejo.jm@gmail.com> July 30, 2013
	 */
	public $controllerName;

	/**
	 * the root off permisions for admin
	 * @var    [type]
	 * @author Alejo  JM <alejo.jm@gmail.com> July 30, 2013
	 */
	public $acoRootAdmin;

	/**
	 * reference to aro logedin, based on LoginType
	 * @var    [type]
	 * @author Alejo  JM <alejo.jm@gmail.com> July 30, 2013
	 */
	public $aroLoginType;

	/**
	 * exclude this one from the results
	 * @var    string
	 * @author Alejo  JM <alejo.jm@gmail.com> July 30, 2013
	 */
	public $defaultExcludeActions = "'view','edit','delete'";

	/**
	 * query to exlude items for the menu
	 * @var    string
	 * @author Alejo JM <alejo.jm@gmail.com> February 07, 2014
	 */
	protected $excludeActions = " NOT IN (NOT_IN_ACTIONS) ";

	/**
	 * array contain exlude action can be manipulated from the controllers
	 * @var    array
	 * @author Alejo JM <alejo.jm@gmail.com> February 07, 2014
	 */
	static $excludeInActions = array();

	/**
	 * query to exlude controllers for the menu
	 * @var    string
	 * @author Alejo JM <alejo.jm@gmail.com> September 04, 2014
	 */
	protected $excludeControllers = " AND Acos.alias NOT IN (NOT_IN_CTRL) ";

	/**
	 * array contain exlude action can be manipulated from the controllers
	 * @var    array
	 * @author Alejo JM <alejo.jm@gmail.com> February 07, 2014
	 */
	static $excludeInControllers = array();

	/**
	 * add controllers from controller actions
	 * @var    array
	 * @author Alejo JM <alejo.jm@gmail.com> September 04, 2014
	 */
	static $addControllers = array();

	/**
	 * add controllers query
	 * @var    array
	 * @author Alejo JM <alejo.jm@gmail.com> September 04, 2014
	 */
	protected $addControllersQuery = '0';

	/**
	 * cake request plugin param
	 * @var    string
	 * @author Alejo JM <alejo.jm@gmail.com> April 16, 2015
	 */
	protected $pluginName = '';


	/**
	 * relations keeping queries easy
	 * @var    array
	 * @author Alejo JM <alejo.jm@gmail.com> July 30, 2013
	 */
	public $hasOne = array(
		'Aco' => array(
			'foreignKey' => false,
			'type'       => 'LEFT',
			'conditions' => 'Aco.id=Menu.aco_id'
		),
		'MenuController' => array(
			'className'  =>'Ajm.MenuController',
			'foreignKey' => false,
			'type'       => 'LEFT',
			'conditions' => 'MenuController.id=Aco.parent_id'
		)
	);

	/**
	 * setup instances needed to work
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> July 30, 2013
	 */
	public function setup($Acl, $controllerName, $pluginName)
	{
		$this->Acl            = $Acl;
		$this->controllerName = $controllerName;
		$this->pluginName     = $pluginName;

		$LoginModel = ClassRegistry::init('Login');
		$LoginModel->contain(array('Logintype'));
		$this->login_info = $LoginModel->findById(AuthComponent::user('id'));
	}

	/**
	 * remove actions from the menu
	 * @param  array $actions
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> February 07, 2014
	 */
	public static function removeActions($actions=array())
	{
		Menu::$excludeInActions = Hash::merge(Menu::$excludeInActions, $actions);
	}

	/**
	 * Remove all methods from specific className
	 * take care on base class live the actions in the array validaAction
	 * @param  string $className
	 * @param  array  $validaAction
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> February 07, 2014
	 */
	public static function removeClassMethods($className='', $validaAction=array())
	{
		$frontClass = get_class_methods($className);
		$subClass = get_class_methods(get_parent_class($className));
		$methods = array_diff($frontClass, $subClass,$validaAction);
		Menu::$excludeInActions = Hash::merge(Menu::$excludeInActions, $methods);
	}

	/**
	 * remove controllers from the menu
	 * @param  array  $controllers
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> September 04, 2014
	 */
	public static function removeControllers($controllers=array())
	{
		Menu::$excludeInControllers = $controllers;
	}

	public static function addControllers($controllers=array())
	{
		Menu::$addControllers = $controllers;
	}

	/**
	 * add the excludeInActions to the query
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> February 07, 2014
	 */
	protected function renderExcludeActions()
	{
		$excludeActions = '';
		if(!empty(Menu::$excludeInActions))
			$excludeActions = ",'".implode("','", Menu::$excludeInActions)."'";

		$excludeActionsAll = $this->defaultExcludeActions.$excludeActions;
		$this->excludeActions = str_replace('NOT_IN_ACTIONS', $excludeActionsAll, $this->excludeActions);
	}

	/**
	 * add the excludeInActions to the query
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> February 07, 2014
	 */
	protected function renderExcludeControllers()
	{
		if(!empty(Menu::$excludeInControllers)){
			$excludeControllers = "'".implode("','", Menu::$excludeInControllers)."'";
			$this->excludeControllers = str_replace('NOT_IN_CTRL', $excludeControllers, $this->excludeControllers);
			return;
		}
		$this->excludeControllers = ' AND 1=1 ';
	}

	/**
	 * generate the query for new controllers
	 * @return void
	 * @author Alejo JM <alejo.jm@gmail.com> September 04, 2014
	 */
	protected function renderAddControllers()
	{
		if(!empty(Menu::$addControllers)){
			$newControllers = "'".implode("','", Menu::$addControllers)."'";
			$this->addControllersQuery = "Aco.alias IN ($newControllers) AND MenuController.alias {$this->excludeActions} AND MenuController.alias <> 'add'";
		}
	}

	/**
	 * get menu for logein user
	 * @param  array $login_info
	 * @param  Object $Acl
	 * @return array
	 * @author Alejo  JM          <alejo.jm@gmail.com> July 30, 2013
	 */
	public function getItemsForUser()
	{
		$this->Acl->Aco->recursive = 0;

		$this->acoRootAdmin = $this->Acl->Aco->findByAlias(!empty($this->pluginName) ? $this->pluginName : $this->controllerName);

		$this->Acl->Aro->recursive = 0;
		$this->aroLoginType = $this->Acl->Aro->findByAlias($this->login_info['Logintype']['title']);

		$this->renderExcludeActions();
		$this->renderExcludeControllers();
		$this->renderAddControllers();

		$userHome = $this->login_info['Logintype']['home'];

		//home user ? get all controllers for the user can navigate
		if($_SERVER['REQUEST_URI'] == $userHome){
			$items = $this->getControllersForUser();
			$items['home_user'] = $userHome;
		} else {
			$items = $this->getActionsForUser();
		}

		return $items;
	}

	/**
	 * get all user controllers can execute
	 * @return [type] [description]
	 * @author Alejo  JM            <alejo.jm@gmail.com> July 30, 2013
	 */
	public function getControllersForUser()
	{
		$conditions = '';
		//admin can edit everything
		if($this->login_info['Logintype']['title'] == 'Admin')
		{
			$this->hasOne['Acos']['type'] = '';
			$this->hasOne['Acos']['conditions'] = '`Menu`.`aco_id`=1';
			$items = $this->find('all', array(
				'conditions'=>"Acos.parent_id = {$this->acoRootAdmin['Aco']['id']} ".$this->excludeControllers,
				'fields'=> 'Acos.alias AS controller, MenuController.alias AS method',
				'group'=>'Acos.alias',
				'order'=>'Acos.alias ASC'
			));

			return $items;
		}

		//the rest get only your controllers
		$items = $this->find('all', array(
			'conditions'=>"MenuController.id IS NOT NULL AND Menu.aro_id = {$this->aroLoginType['Aro']['id']} AND Aco.alias ".$this->excludeActions.$this->excludeControllers,
			'fields'=>'MenuController.alias AS controller, Aco.alias AS method',
			'group'=>'MenuController.alias',
			'order'=>'MenuController.alias ASC'
		));

		return $items;
	}

	/**
	 * get all user actions can execute
	 * @return [type] [description]
	 * @author Alejo  JM            <alejo.jm@gmail.com> July 30, 2013
	 */
	public function getActionsForUser()
	{
		//admin get all childrens, no matter what
		if($this->login_info['Logintype']['title'] == 'Admin'){
			$controller = $this->Aco->findByAlias($this->controllerName);
			if(empty($controller))
				return array();

			$items = $this->MenuController->find('all', array(
				'conditions'=>"MenuController.parent_id = {$controller['Aco']['id']} AND MenuController.alias {$this->excludeActions} OR ({$this->addControllersQuery})",
				'fields'=> 'Aco.alias AS controller, MenuController.alias AS method',
				'order'=>'MenuController.alias ASC, Aco.alias ASC'
			));

			$moreItems = $this->getRelationsAdminModel();
			return array_merge($items, $moreItems);
		}

		//get only the actions for current controller
		$items = $this->find('all', array(
			'conditions'=>"Menu.aro_id = {$this->aroLoginType['Aro']['id']} AND MenuController.alias = '{$this->controllerName}' AND Aco.alias ".$this->excludeActions,
			'fields'=>'MenuController.alias AS controller, Aco.alias AS method',
			'order'=>'MenuController.alias ASC'
		));

		$moreItems = $this->getRelationsModel();
		return array_merge($items, $moreItems);
	}

	/**
	 * get model relations for admin, keep all information together
	 * @return array more items for the menu
	 */
	private function getRelationsAdminModel()
	{
		$modelName = Inflector::singularize($this->controllerName);
		$currentModel = ClassRegistry::init($modelName);
		if(strpos(get_parent_class($currentModel), $this->pluginName) === false){
			return array();
		}

		$items = $this->MenuController->find('all', array(
			'conditions'=>"MenuController.parent_id = {$this->acoRootAdmin['Aco']['id']} AND MenuController.alias ".$this->excludeActions,
			'fields'=> 'MenuController.alias AS controller, Aco.alias AS method',
			'order'=>'MenuController.alias ASC'
		));

		$moreItems = array();
		$associations = $currentModel->getAssociated();

		foreach ($items as $key => $menu) {
			$modelName = Inflector::singularize($menu['MenuController']['controller']);
			if(isset($associations[$modelName])){
				$menu['Aco']['method'] = 'index';
				array_push($moreItems, $menu);
			}
		}

		return $moreItems;
	}


	/**
	 * get model relations for logged in user, keep all information together
	 * @return array more items for the menu
	 */
	private function getRelationsModel()
	{
		$modelName = Inflector::singularize($this->controllerName);
		$currentModel = ClassRegistry::init($modelName);

		if(get_parent_class($currentModel) != 'AdminAppModel'){
			return array();
		}
		$conditions = "Menu.aro_id = {$this->aroLoginType['Aro']['id']} AND Aco.alias ".$this->excludeActions;
		$items = $this->find('all', array(
			'conditions'=>$conditions,
			'fields'=>'MenuController.alias AS controller, Aco.alias AS method',
			'order'=>'MenuController.alias ASC'
		));

		$moreItems = array();
		$associations = $currentModel->getAssociated();
		foreach ($items as $key => $menu) {
			$modelName = Inflector::singularize($menu['MenuController']['controller']);
			if(isset($associations[$modelName])){
				array_push($moreItems, $menu);
			}
		}

		return $moreItems;
	}

}
