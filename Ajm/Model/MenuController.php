<?php
App::uses('AppModel', 'Model');
/**
 * MenuController Model, uses acos table
 *
 */
class MenuController extends AppModel {

	public $actsAs=array('Containable');
	public $useTable = 'acos';

	/**
	 * relations keeping queries easy
	 * @var    array
	 */
	public $hasOne = array(
		'Aco' => array(
			'foreignKey' => false,
			'type'       => 'LEFT',
			'conditions' => 'MenuController.parent_id=Aco.id'
		),
	);
}
