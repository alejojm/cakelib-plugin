<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<div class="<?php echo $pluralVar; ?> view bake-view">
<h2><?php echo "<?php echo __('{$singularHumanName}'); ?>"; ?></h2>
	<dl class="">
<?php
foreach ($fields as $field) {
	$isKey = false;
	if (!empty($associations['belongsTo'])) {
		foreach ($associations['belongsTo'] as $alias => $details) {
			if ($field === $details['foreignKey']) {
				$isKey = true;
				echo "\t\t<dt class=\"control-label\"><?php echo __('" . Inflector::humanize(Inflector::underscore($alias)) . "'); ?></dt>\n";
				echo "\t\t<dd class=\"form-control\">\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t&nbsp;\n\t\t</dd>\n";
				break;
			}
		}
	}
	if ($isKey !== true) {
		echo "\t\t<dt class=\"control-label\"><?php echo __('" . Inflector::humanize($field) . "'); ?></dt>\n";
		if($this->templateVars['schema'][$field]['type'] == 'boolean' && $this->templateVars['schema'][$field]['length'] == 1){
			echo "\t\t<?php if(\${$singularVar}['{$modelClass}']['{$field}']):?>\n";
			echo "\t\t\t<dd><span class=\"glyphicon glyphicon-ok-circle\"></span></dd>\n";
			echo "\t\t<?php else:?>\n";
			echo "\t\t\t<dd><span class=\"glyphicon glyphicon-remove-circle\"></span></dd>\n";
			echo "\t\t<?php endif;?>\n";
		} else {
			echo "\t\t<dd class=\"form-control\">\n\t\t\t<?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>\n\t\t\t&nbsp;\n\t\t</dd>\n";
		}

	}
}
?>
	</dl>
</div>
<?php

if (!empty($associations['hasOne'])) :
	foreach ($associations['hasOne'] as $alias => $details): ?>
<div class="related bake-view">
	<h3><?php echo "<?php echo __('Related " . Inflector::humanize($details['controller']) . "'); ?>"; ?></h3>
	<?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])): ?>\n"; ?>
		<dl>
	<?php
			foreach ($details['fields'] as $field) {
				echo "\t\t<dt class=\"control-label\"><?php echo __('" . Inflector::humanize($field) . "'); ?></dt>\n";
				echo "\t\t\t<dd class=\"form-control\">\n";
				echo "\t\t\t\t<?php echo \${$singularVar}['{$alias}']['{$field}']; ?>\n";
				echo "\t\t\t\t&nbsp;\n\t\t\t</dd>\n\t";
			}
	?>
	</dl>
	<?php echo "<?php endif; ?>\n"; ?>
</div>
	<?php
	endforeach;
endif;
if (empty($associations['hasMany'])) {
	$associations['hasMany'] = array();
}
if (empty($associations['hasAndBelongsToMany'])) {
	$associations['hasAndBelongsToMany'] = array();
}
$relations = array_merge($associations['hasMany'], $associations['hasAndBelongsToMany']);
foreach ($relations as $alias => $details):
	$otherSingularVar = Inflector::variable($alias);
	$otherPluralHumanName = Inflector::humanize($details['controller']);
	?>
<div class="related bake-view table-responsive">
	<h3><?php echo "<?php echo __('Related " . $otherPluralHumanName . "'); ?>"; ?></h3>
	<?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])): ?>\n"; ?>
	<table class="table table-striped bake-table" cellpadding = "0" cellspacing = "0">
	<tr>
<?php
			foreach ($details['fields'] as $field) {
				echo "\t\t<th><?php echo __('" . Inflector::humanize($field) . "'); ?></th>\n";
			}
?>
		<th class="actions"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
	</tr>
<?php
echo "\t<?php foreach (\${$singularVar}['{$alias}'] as \${$otherSingularVar}): ?>\n";
		echo "\t\t<tr>\n";
			foreach ($details['fields'] as $field) {
					if($field == 'created' || $field == 'updated')
						echo "\t\t<td><?php echo ucFirst(strftime('%b%e/%y',strtotime(\${$singularVar}['{$modelClass}']['{$field}']))); ?>&nbsp;</td>\n";
					else if($details['schema'][$field]['type'] == 'boolean' && $details['schema'][$field]['length'] == 1){
						echo "\t\t<?php if(\${$otherSingularVar}['{$field}']):?>\n";
						echo "\t\t\t<td><span class=\"glyphicon glyphicon-ok-circle\"></span></td>\n";
						echo "\t\t<?php else:?>\n";
						echo "\t\t\t<td><span class=\"glyphicon glyphicon-remove-circle\"></span></td>\n";
						echo "\t\t<?php endif;?>\n";
					}
					else
						echo "\t\t\t<td><?php echo \${$otherSingularVar}['{$field}']; ?></td>\n";
			}

			echo "\t\t\t<td class=\"actions\">\n";
			echo "\t\t\t<?php echo \$this->Html->link('<span class=\"glyphicon glyphicon-eye-open\"></span>', array('controller' => '{$details['controller']}', 'action' => 'view', \${$otherSingularVar}['{$details['primaryKey']}']),array('escape'=>false, 'class'=>'btn btn-xs btn-default', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>__('View'))); ?>\n";
			echo "\t\t\t<?php echo \$this->Html->link('<span class=\"glyphicon glyphicon-pencil\"></span>', array('controller' => '{$details['controller']}', 'action' => 'edit', \${$otherSingularVar}['{$details['primaryKey']}']),array('escape'=>false,'class'=>'btn btn-xs btn-info', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>__('Edit'))); ?>\n";
			echo "\t\t\t<?php echo \$this->Html->link('<span class=\"glyphicon glyphicon-trash\"></span>', array('controller' => '{$details['controller']}', 'action' => 'delete', \${$otherSingularVar}['{$details['primaryKey']}']), array('escape'=>false, 'class'=>'btn btn-xs btn-danger', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>__('Delete'))); ?>\n";
			echo "\t\t\t</td>\n";
		echo "\t\t</tr>\n";

echo "\t<?php endforeach; ?>\n";
?>
	</table>
<?php echo "<?php endif; ?>\n\n"; ?>
</div>
<?php endforeach; ?>
