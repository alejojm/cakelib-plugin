<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<div class="<?php echo $pluralVar; ?> bake-form">
	<h2 class="sub-header"><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></span></h2>
	<div class="alert alert-danger">
		<?php echo "<?php echo __('Are you sure you want to delete # %s?', \$this->params['pass'][0]); ?>\n"; ?>
	</div>
<?php echo "\t<?php\n";
	echo "\t\techo \$this->Form->create('{$modelClass}');
	\techo \$this->Form->hidden('id', array('value'=>\$this->params['pass'][0]));
	\techo \$this->Html->link(__('Cancel'), array('action'=>'index'), array('class'=>'btn btn-default pull-left'));
	\techo \$this->Form->end(array('label'=>__('Delete'), 'class'=>'btn btn-danger pull-left'));
	";
	echo "?>\n";
?>
</div>