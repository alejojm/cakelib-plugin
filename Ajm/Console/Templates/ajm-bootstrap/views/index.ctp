<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<div class="<?php echo $pluralVar; ?> bake-index">
	<h2 class="sub-header"><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?>&nbsp;<span class="badge"><?php echo "<?php echo \$this->Paginator->counter(array('format' => __('{:count}'))); ?>"?></span></h2>
	<div class="index-search row">
		<div class="col-md-6">
			<p>
				<form action="<?php echo "<?php echo str_replace('page', 'search', \$this->here); ?>"; ?>" method="get" accept-charset="utf-8">
					<div class="input-group">
						<input class="form-control" type="text" name="q" value="<?php echo "<?php echo isset(\$this->params->query['q']) ? \$this->params->query['q'] : ''; ?>" ?>" id="search_index_view">
						<span class="input-group-btn">
							<input class="btn btn-default" type="submit" value="<?php echo __('Search') ?>">
						</span>
					</div>
				</form>
			</p>
			<br>
		</div>
		<div class="col-md-6">

		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-striped bake-table" cellpadding="0" cellspacing="0">
		<thead>
			<tr><?php foreach ($fields as $field): ?>
				<?php if($modelObj && $modelObj->displayFields): ?>
						<?php if (in_array($field, $modelObj->displayFields)): ?>
							<?php echo "\n\t\t\t\t<th><?php echo \$this->Paginator->sort('{$field}'); ?></th>"; ?>
						<?php endif ?>
				<?php else: ?>
					<?php echo "\n\t\t\t\t<th><?php echo \$this->Paginator->sort('{$field}'); ?></th>"; ?>
				<?php endif; ?>
			<?php endforeach; ?>

				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		<?php
		echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
		echo "\t\t<tr>\n";
			foreach ($fields as $field) {
				$continue = true;
				$isKey = false;
				if (!empty($associations['belongsTo'])) {
					foreach ($associations['belongsTo'] as $alias => $details) {
						if ($field === $details['foreignKey']) {
							$isKey = true;
							echo "\t\t\t<td>\n\t\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t</td>\n";
							break;
						}
					}
				}

				if ($isKey !== true) {
					if($modelObj && $modelObj->displayFields){
						if (!in_array($field, $modelObj->displayFields))
							$continue = false;
					}

					if(!$continue) continue;

					if($field == 'created' || $field == 'updated')
						echo "\t\t\t<td><?php echo ucFirst(strftime('%b %e/%y',strtotime(\${$singularVar}['{$modelClass}']['{$field}']))); ?>&nbsp;</td>\n";
					else if($this->templateVars['schema'][$field]['type'] == 'boolean' && $this->templateVars['schema'][$field]['length'] == 1){
						echo "\t\t\t<td><span class=\"glyphicon <?php echo \${$singularVar}['{$modelClass}']['{$field}'] ? 'glyphicon-ok-circle' : 'glyphicon-remove-circle' ?>\"></span></td>\n";
					}
					else
						echo "\t\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
				}
			}

			echo "\t\t\t<td class=\"actions\">\n";
			echo "\t\t\t\t<?php echo \$this->Html->link('<span class=\"glyphicon glyphicon-eye-open\"></span>', array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape'=>false, 'class'=>'btn btn-xs btn-default', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>__('View'))); ?>\n";
			echo "\t\t\t\t<?php echo \$this->Html->link('<span class=\"glyphicon glyphicon-pencil\"></span>', array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape'=>false,'class'=>'btn btn-xs btn-info', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>__('Edit'))); ?>\n";
			echo "\t\t\t\t<?php echo \$this->Html->link('<span class=\"glyphicon glyphicon-trash\"></span>', array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape'=>false, 'class'=>'btn btn-xs btn-danger', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>__('Delete'))); ?>\n";
			echo "\t\t\t</td>\n";
		echo "\t\t</tr>\n";
		echo "\t\t<?php endforeach; ?>\n";
		?>
		</tbody>
		</table>
	</div>
	<div class="alert alert-info">
		<?php echo "<?php
			echo \$this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
		?>";
		?>

	</div>
	<div class="pagination">
		<?php echo "<?php\n";
			echo "\t\t\techo \$this->Paginator->prev('<span class=\"glyphicon glyphicon-chevron-left\"></span>', array('tag' => 'li', 'escape'=>false), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));\n";
			echo "\t\t\techo \$this->Paginator->numbers(array('separator' => '', 'tag'=>'li', 'currentTag'=>'a', 'currentClass'=>'active'));\n";
			echo "\t\t\techo \$this->Paginator->next('<span class=\"glyphicon glyphicon-chevron-right\"></span>', array('tag' => 'li','escape'=>false), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));\n";
			echo "\t\t?>\n";
		?>
	</div>
</div>