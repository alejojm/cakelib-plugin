<ul class="nav nav-sidebar">
	<?php

	/**
	 * Render itemsMenu, was setup by appAdminController
	 */
	if(isset($itemsMenu['home_user'])){
		$homeUserInfo = explode('/', str_replace('/'.$this->params->plugin.'/', '', $itemsMenu['home_user']));
		unset($itemsMenu['home_user']);
	}

	foreach ($itemsMenu as $key => $item):
		$itemValues = array_values($item);
		$controller = $itemValues[0];
		$action     = $itemValues[1];
		empty($action['method']) ? $action['method'] = 'index' : null;
		if($action['method'] == 'add')
			$title = 'New '.Inflector::humanize(Inflector::singularize($controller['controller']));
		else
			$title = Inflector::humanize($controller['controller']).($action['method'] == 'index' ? '' : ' '.Inflector::humanize($action['method']));


		$active = strtoupper($this->params->controller) == strtoupper($controller['controller']) && strtoupper($this->params->action) == strtoupper($action['method']) ? 'active' : '';
		if(in_array($this->params->action, array('view', 'edit')) && strtoupper($this->params->controller) == strtoupper($controller['controller']) && $action['method']=='index'){
			$active = 'active';
		}

		//home user don't have active one
		$active = @$homeUserInfo[0] == $controller['controller'] && @$homeUserInfo[1] == $action['method'] ? '' : $active;
		$active .= strpos($action['method'], 'add') !== false ? ' add_active ' : '';

	?>
		<li class="<?php echo $active ?>"><?php echo $this->Html->link(__($title), array('controller' => $controller['controller'], 'action' => $action['method'])); ?></li>
	<?php endforeach;

	/**
	 * render addItems params, this element can have params like this
	 * echo $this->element('Menu', array( 'addItems'=>array(
	 *     array('title'=>'some_title', 'action'=>'someAction', 'pass'=>'optional') or
	 *     array('title'=>'some_title', 'link'=>'http://somelink.com'
	 * )));
	 */
	if(isset($addItems)) foreach ($addItems as $k => $addItem):
		$controller = isset($addItem['controller']) ? $addItem['controller'] : $this->name;
		$pass       = isset($addItem['pass']) ? $addItem['pass'] : null;
		$message    = isset($addItem['message']) ? $addItem['message'] : null;

		if(isset($addItem['link'])){
			echo '<li>'.$this->Html->link(__($addItem['title']), $addItem['link'], array('target'=>'_blank')).'</li>';
			continue;
		}

		//dont make a menu if same controller same action as item
		if($controller == $this->name && $addItem['action'] == $this->action)
			continue;

		if ($message):
	?>
		<li><?php echo $this->Form->postLink(__($addItem['title']), array('controller' => $controller, 'action' => $addItem['action'], $pass), null, $message); ?></li>
	<?php else: ?>
		<li><?php echo $this->Html->link(__($addItem['title']), array('controller' => $controller, 'action' => $addItem['action'], $pass), array('class'=>$addItem['title'])); ?></li>
	<?php
		endif;
	endforeach;?>
</ul>
