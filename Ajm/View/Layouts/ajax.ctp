<?php

	if(isset($response))
		$results = $response;

	if(strlen($this->fetch('content')) > 0)
		$results['html'] = $this->fetch('content');

	if(isset($results))
		echo json_encode(array('json'=>$results));

	if(Configure::read('debug') > 1)
    	echo $this->element('sql_dump');
 ?>
